class ApplicationMailer < ActionMailer::Base
  default from: 'reg@gia66.ru'
  layout 'mailer'
end
