class AnswersImport
  include ActiveModel::Model
  require 'roo'

  attr_accessor :file

  def initialize(attributes={})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def load_imported_answers
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(5)
    (6..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      result = Item.find_by_id(row["id"]) || Item.new
      result.attributes = row.to_hash
      result
    end
  end

  def imported_answers
    @imported_answers ||= load_imported_answers
  end

  def save
    if imported_answers.map(&:valid?).all?
      imported_answers.each(&:save!)
      true
    else
      imported_answers.each_with_index do |result, index|
        result.errors.full_messages.each do |msg|
          errors.add :base, "Row #{index + 6}: #{msg}"
        end
      end
      false
    end
  end


  def importing(file, current_user)
    result = ''
    begin
      if !file.nil?
        xlsx = Roo::Spreadsheet.open(file.tempfile)

        if 1 == 1
          subject_last = nil
          subject_id_last = nil
          test_type_last = nil
          test_type_id_last = nil
          # Импорт ответов
          attribute_list =           ['-',  '-',                '-',    'identifier',   'examdate',  'task_number', 'answer_value', 'replace_value']

          if xlsx.sheet(0).row(1) == ['ID', 'Тип тестирования', 'Предмет', 'Идентификатор', 'Дата', 'Номер ответа',   'Ответ',      'Замена ответа']
            i = 1
            while i<xlsx.sheet(0).last_row
              i = i + 1
              xll = xlsx.sheet(0).row(i)
              j = 0
              m = Answer.where(:id => xll[0]).first
              sozdali = false
              if m.nil?
                m = Answer.where(:identifier => xll[3], :examdate => xll[4], :task_number => xll[5]).first
                if m.nil?
                  m = Answer.new
                  sozdali = true
                end
              end

              begin
                # puts("!!!!!!!!!!!!!! #{xll[1]} !!!!!!!!!!!")
                # m.test_type_id = TestType.where(:name => xll[1]).first.id
                # m.subject_id = Subject.where(:subject_name => xll[2]).first.id
                if test_type_id_last == xll[1]
                  m.test_type_id = test_type_last
                else
                  m.test_type_id = TestType.where(:name => xll[1]).first.id
                  test_type_last = m.test_type_id
                  test_type_id_last = xll[1]
                end
                # puts("string 76")
                if subject_id_last == xll[2]
                  m.subject_id = subject_last
                else
                  m.subject_id = Subject.where(:subject_name => xll[2]).first.id
                  subject_last = m.subject_id
                  subject_id_last = xll[2]
                end


                while j<attribute_list.count
                  if m.has_attribute?(attribute_list[j].to_sym)
                    m.set_property(attribute_list[j], xll[j])
                  end
                  j = j + 1
                end

                if 2 == 2
                  if m.save
                    if sozdali == true
                      result = result + "<tr><td>#{i-1}</td><td>#{xll[2]}</td><td>Удалось, создали</td></tr>"
                    else
                      result = result + "<tr><td>#{i-1}</td><td>#{xll[2]}</td><td>Удалось, обновили</td></tr>"
                    end
                  else
                    result = result + "<tr><td>#{i-1}</td><td>#{xll[2]}</td><td>Не удалось: "
                    m.errors.full_messages.each do |message|
                      result = result + "<li>#{message}</li>"
                    end
                    result = result + "</td></tr>"
                  end

                else
                  result = result + "<tr><td>#{i-1}</td><td>#{xll[2]}</td><td>Не удалось на уровне базы!!: #{m}</td></tr>"
                end
              rescue
                result = result + "<tr><td>#{i-1}</td><td>#{xll[2]}</td><td>Не удалось"
              end


            end
          else
            result = "Таблица не соответствует требованиям.<br>#{xlsx.sheet(0).row(1)}<br>Вместо<br>'ID', 'Тип тестирования', 'Код предмета', 'Идентификатор', 'Дата тестирования', 'Номер ответа', 'Ответ', 'Замена ответа'"
          end
          result = "<table class='table-stripped'><thead><th>Строка №</th><th> - - Запись - -</th><th>Результат загрузки</th>#{result}</table>"
          return result
        end
        return ("Файл не определён")
      end
    rescue
      return ("Файл вызвал общую ошибку загрузки на уровне базы данных. Сделайте заявку на admin@edzsoft.ru с приложением файла.")
    end
  end


end