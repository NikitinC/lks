class TeacherStatementsImport
  include ActiveModel::Model
  require 'roo'

  attr_accessor :file

  def initialize(attributes={})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def load_imported_task_specifications
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(5)
    (6..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      task_specification = Item.find_by_id(row["id"]) || Item.new
      task_specification.attributes = row.to_hash
      task_specification
    end
  end

  def imported_task_specifications
    @imported_task_specifications ||= load_imported_task_specifications
  end

  def save
    if imported_task_specifications.map(&:valid?).all?
      imported_task_specifications.each(&:save!)
      true
    else
      imported_task_specifications.each_with_index do |task_specification, index|
        task_specification.errors.full_messages.each do |msg|
          errors.add :base, "Row #{index + 6}: #{msg}"
        end
      end
      false
    end
  end

end