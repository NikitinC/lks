class EgeStatement < ApplicationRecord
  acts_as_paranoid
  has_paper_trail


  #============================VALIDATION START================================
  # name, code, created_at, updated_at, short_name, fisgia9code, fisgia11code
  # validates  :code,
  #            :name,
  #            :short_name,
  #            :presence => true

  #=============================VALIDATION END=================================

  # has_many :ous
  # has_many :shedule_tasks
  # has_many :monitoring_objectivity_ates
  # has_many :appeal_stations

  belongs_to :user
  belongs_to :ate, optional: true
  belongs_to :document_type, optional: true
  belongs_to :citizenship, optional: true
  belongs_to :ege_participant_category, optional: true
  belongs_to :registration_place, optional: true

  mount_uploader :education_scan, ScansUploader

  def self.search(search)
    if search
      where('name || cast(code as varchar(5)) LIKE ?', "%#{search}%")
    end
  end

  def to_s
    short_name
  end

  def set_property(prop_name, prop_value)
    self.send("#{prop_name}=",prop_value)
    # puts("#{prop_name} : #{prop_value}")
    # puts self.inspect
  end

  def formatted_date
    if !self.birthday.nil?
      return self.birthday.strftime("%d.%m.%Y")
    else
      return ("01.01.2001")
    end

  end

  def formatted_kogda
    if !self.document_vydan_kogda.nil?
      return self.document_vydan_kogda.strftime("%d.%m.%Y")
    else
      return ("01.01.2001")
    end
  end

  self.per_page = 20

  # set per_page globally
  WillPaginate.per_page = 20

end

