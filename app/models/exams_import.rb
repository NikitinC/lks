class ExamsImport
  include ActiveModel::Model
  require 'roo'

  attr_accessor :file

  def initialize(attributes={})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def load_imported_exams
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(5)
    (6..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      exam = Item.find_by_id(row["id"]) || Item.new
      exam.attributes = row.to_hash
      exam
    end
  end

  def imported_exams
    @imported_exams ||= load_imported_exams
  end

  def save
    if imported_exams.map(&:valid?).all?
      imported_exams.each(&:save!)
      true
    else
      imported_exams.each_with_index do |exam, index|
        exam.errors.full_messages.each do |msg|
          errors.add :base, "Row #{index + 6}: #{msg}"
        end
      end
      false
    end
  end

  def stophone(s)
    s = s.gsub(/[^0-9]/, '')
    if s.length > 10
      s = s[-10..-1] || s
    end
    s = "+7(#{s[0..2]})#{s[3..5]}-#{s[6..9]}"
    return s
  end

  def importing(file, current_user)
    result = ''
    if 1 == 1
      if !file.nil?
        xlsx = Roo::Spreadsheet.open(file.tempfile)

        if xlsx.sheets.count == 1

          # Импорт онлайн-протоколоы
          attribute_list = ['code', '-', '-', '-', 'examdate', 'name', 'short_name', '-', 'fisgia11code']
          if xlsx.sheet(0).row(1) == ['Код', 'Форма ГИА', 'Этап', 'Предмет', 'Дата', 'Наименование', 'Краткое наименование', 'Резерв', 'ExamGlobalID']
            i = 1
            while i<xlsx.sheet(0).last_row
              i = i + 1
              next if "#{xlsx.sheet(0).row(i)[1]}" != 'ЕГЭ'
              j = 0
              subject = Subject.where("subject_name = '#{xlsx.sheet(0).row(i)[3]}'").first
              m = Exam.where(:subject_id => subject.id, :examdate => xlsx.sheet(0).row(i)[4]).first
              sozdali = false
              if m.nil?
                m = Exam.new
                sozdali = true
              end

              while j<attribute_list.count
                if m.has_attribute?(attribute_list[j].to_sym)
                  m.set_property(attribute_list[j], xlsx.sheet(0).row(i)[j])
                end
                j = j + 1
              end

              if 2 == 2
                m.code = m.fisgia11code.to_s

                m.short_name = "#{subject.subject_name[0..2]} (#{m.formatted_date})"
                m.name = "#{subject.subject_name} (#{m.formatted_date})"
                m.exam_wave_id = ExamWave.where("name = '#{xlsx.sheet(0).row(i)[2]}'").first.id
                m.subject_id = subject.id
                m.is_reserv = true if "#{xlsx.sheet(0).row(i)[7]}" == 'да'
                m.is_reserv = false if "#{xlsx.sheet(0).row(i)[7]}" == 'нет'

                if m.save
                  if sozdali == true
                    result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[3]} (#{xlsx.sheet(0).row(i)[4]})</td><td>Удалось, создали</td></tr>"
                  else
                    result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[3]} (#{xlsx.sheet(0).row(i)[4]})</td><td>Удалось, обновили</td></tr>"
                  end
                else
                  result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[3]} (#{xlsx.sheet(0).row(i)[4]})</td><td>Не удалось: "
                  m.errors.full_messages.each do |message|
                    result = result + "<li>#{message}</li>"
                  end
                  result = result + "</td></tr>"
                end

              else

                result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[3]} (#{xlsx.sheet(0).row(i)[4]})</td><td>Не удалось на уровне базы!!: #{m}</td></tr>"
              end

            end

          end
          result = "<table class='table-stripped'><thead><th>Строка №</th><th> - - Запись - -</th><th>Результат загрузки</th>#{result}</table>"
          return result
        end
      return ("Файл не определён")
      end
    else
      return ("Файл вызвал общую ошибку загрузки на уровне базы данных. Сделайте заявку на support с приложением файла.")
    end
  end


end