# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= User.new # guest user (not logged in)

    if user.admin?
      can :manage, :all
    else
      can :read, :all
      cannot :manage, TeacherStatement
      cannot :manage, Appeal
      cannot :manage, Result
      cannot :manage, EgeStatement
      can :create, TeacherStatement
      can :create, EgeStatement

      can [:index, :view, :read, :update, :edit, :create, :delete, :destroy], EgeStatement, user_id: user.id
      cannot [:edit, :delete, :destroy], EgeStatement, processed: true
      can [:index, :view, :read, :update, :edit, :create, :delete, :destroy], TeacherStatement, user_id: user.id
    end

    if user.dpo_role?
      can :manage, TeacherStatement
      cannot :import, TeacherStatement
    end

    if user.ous_role?
      cannot :manage, Ou
      can :manage, Ou, :code => user.target_id
      cannot :manage, Ate
      cannot :manage, Mouo
      cannot :manage, AppealStation
      cannot :manage, ProcessCondition
      cannot :manage, Subject
      cannot :manage, TaskSpecification
      cannot :manage, UserTestType
      cannot :manage, AppealCondition
      cannot :manage, AppealType
      cannot :manage, Answer
      cannot :manage, Notification
      can :view, Notification
      cannot :manage, TestType
      # AppealCondition
      cannot :manage, Appeal
      can :create, Appeal
      can [:index, :view, :read, :update, :edit, :create, :delete, :destroy], Appeal, user_id: user.id
      cannot :manage, Result
    end

    if user.mouo_role?
      user_mouo = Mouo.where(:code => user.target_id).first
      mouo_ids = user_mouo.id
      mouo_code = user_mouo.code
      ate_ids = Ate.where(code: mouo_code)
      ou_ids = Ou.where(ate_id: ate_ids).map(&:id)
      station_ids = AppealStation.where(mouo_id: mouo_ids).map(&:id)

      cannot :manage, Ou
      cannot :manage, Ate
      cannot :manage, Mouo
      can :manage, Ou, :ate_id => user.target_id
      can :manage, Ate, :code => user.target_id
      can :manage, Mouo, :code => user.target_id
      cannot :manage, AppealStation
      cannot :manage, ProcessCondition
      cannot :manage, Subject
      cannot :manage, TaskSpecification
      cannot :manage, UserTestType
      cannot :manage, AppealCondition
      cannot :manage, AppealType
      cannot :manage, Answer
      cannot :manage, Notification
      can :view, Notification
      cannot :manage, TestType
      cannot :manage, Appeal
      can :create, Appeal
      can [:index, :view, :read, :update, :edit, :create, :delete, :destroy], Appeal, user_id: user.id
      can [:index, :view, :read, :update, :edit, :create, :delete, :destroy], Appeal, appeal_station_id: station_ids
      cannot :manage, Result
    end


    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    # can :manage, Announcement
    # can :manage, Notification
    # can :manage, Notification



    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end

  # cannot :manage, Ate

end
