class Anonce < ActiveRecord::Base

  Anonce.table_name = 'announcements'

  def set_property(prop_name, prop_value)
    self.send("#{prop_name}=",prop_value)
    # puts("#{prop_name} : #{prop_value}")
    # puts self.inspect
  end

  self.per_page = 20

  # set per_page globally
  WillPaginate.per_page = 20


  ##################################################################################################
  ### Includes and Extensions ######################################################################


  ##################################################################################################
  ### Associations #################################################################################


  ##################################################################################################
  ### Callbacks ####################################################################################


  ##################################################################################################
  ### Validations ##################################################################################

  

  ##################################################################################################
  ### Scopes #######################################################################################


  ##################################################################################################
  ### Other ########################################################################################


  ##################################################################################################
  ### Class Methods ################################################################################


  ##################################################################################################
  ### Instance Methods #############################################################################


  #########
  protected
#########


#######
  private
  #######


end
