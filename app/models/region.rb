class Region < ApplicationRecord
  acts_as_paranoid
  has_paper_trail

  #============================VALIDATION START================================
  # name, code, created_at, updated_at, short_name, fisgia9code, fisgia11code
  validates  :code,
             :name,
             :short_name,
             :presence => true

  #=============================VALIDATION END=================================

  # has_many :ous
  # has_many :shedule_tasks
  # has_many :monitoring_objectivity_ates
  # has_many :appeal_stations


  def self.search(search)
    if search
      where('name || cast(code as varchar(5)) LIKE ?', "%#{search}%")
    end
  end

  def to_s
    short_name
  end

  def set_property(prop_name, prop_value)
    self.send("#{prop_name}=",prop_value)
    # puts("#{prop_name} : #{prop_value}")
    # puts self.inspect
  end

  self.per_page = 20

  # set per_page globally
  WillPaginate.per_page = 20

end
