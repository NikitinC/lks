class RegistrationPlace < ApplicationRecord
  acts_as_paranoid
  has_paper_trail

  has_rich_text :additional_text

  before_save do
    self.code = "#{self.ate.code}0000"
  end

  include PgSearch::Model
  include RsValidator
  # include FioValidator
  # include EmailValidator
  include PhoneValidator

  belongs_to :ate

  #============================VALIDATION START================================
  validates  :name,
             :phones,
             :address,
             :presence => true

  validates :name,
            :rs => true

  validates :phones,
            :phone => true

  validates :ate_id, presence: true

  #=============================VALIDATION END=================================

  def self.search(search)
    if search
      where('name || cast(address as varchar) || cast(phones as varchar) LIKE ?', "%#{search}%")
    end
  end

  def to_s
    name
  end

  def self.options_for_select
    order("id").map { |e| ["#{e.id} - #{e.short_name}", e.id] }
  end

  def set_property(prop_name, prop_value)
    self.send("#{prop_name}=",prop_value)
  end

  def formatted_body
    innertext = Richtext::CodeBlocks::HtmlService.render(self.additional_text.to_s).html_safe
    # rescue innertext = Richtext::CodeBlocks::HtmlService.latex(innertext).html_safe
    innertext
  end

  self.per_page = 20

  WillPaginate.per_page = 20

end
