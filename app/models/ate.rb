class Ate < ApplicationRecord
  include PgSearch::Model

  has_many :mouos
  has_many :ous
  has_many :teacher_statement

  def self.search(search)
    if search
      where('name || cast(code as varchar(5)) LIKE ?', "%#{search}%")
    end
  end

  def to_s
    "#{name}"
  end

  self.per_page = 20

  # set per_page globally
  WillPaginate.per_page = 20

end
