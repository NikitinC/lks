class Result < ActiveRecord::Base

  belongs_to :subject
  belongs_to :process_condition
  belongs_to :test_type

  def to_s
    "#{subject.subject_name}:#{testresulta}:#{testresultb}:#{testresultc}:#{testresultd}:#{mark100}"
  end

  filterrific :default_filter_params => { :sorted_by => 'created_at_desc' },
              :available_filters => %w[
                sorted_by
                search_query
                with_test_types_id
                with_subject_id
                with_created_at_gte
                results_params
              ]

  scope :search_query, ->(query) {
    return nil  if query.blank?
    query = query.to_s
    # condition query, parse into individual keywords
    terms = query.downcase.split(/\s+/)
    # replace "*" with "%" for wildcard searches,
    # append '%', remove duplicate '%'s
    terms = terms.map { |e|
      (e.gsub('*', '%') + '%').gsub(/%+/, '%')
    }
    # configure number of OR conditions for provision
    # of interpolation arguments. Adjust this if you
    # change the number of OR conditions.
    num_or_conditions = 1
    where(
      terms.map {
        or_clauses = [
          "LOWER(results.blank_2) LIKE ?",
        ].join(' OR ')
        "(#{ or_clauses })"
      }.join(' AND '),
      *terms.map { |e| [e] * num_or_conditions }.flatten
    )
  }

  scope :sorted_by, ->(sort_option) {
    # extract the sort direction from the param value.
    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
    results = Result.arel_table
    test_types = TestType.arel_table
    subjects = Subject.arel_table
    case sort_option.to_s
    when /^created_at_/
      # order("events.created_at #{direction}")
      order(results[:created_at].send(direction))
    when /^test_types/
      Result.joins(:test_type).order(test_type[:name].lower.send(direction))
    when /^subjects/
      Result.joins(:subjects).order(subject[:subject_name].lower.send(direction))
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :results_params, -> (results_params_attr) {
    results = Result.all
    if !results_params_attr.test_types.nil? and results_params_attr.test_types != ''
      results = results.where(test_type: { id: results_params_attr.test_types }).joins(:test_type).distinct
    end
    if !results_params_attr.subjects.nil? and results_params_attr.subjects != ''
      results = results.where(subject: { id: results_params_attr.subjects }).joins(:subject).distinct
    end
    return results
  }

  def self.options_for_sorted_by
    [
      ['По названию (а-я)', 'name_asc'],
      ['По дате появления (сначала новые)', 'created_at_desc'],
      ['По дате появления (сначала старые)', 'created_at_asc'],
      ['По типам тестирований (а-я)', 'test_types_asc'],
      ['По предметам (а-я)', 'subjects_asc']
    ]
  end

  def set_property(prop_name, prop_value)
    self.send("#{prop_name}=",prop_value)
    # puts("#{prop_name} : #{prop_value}")
    # puts self.inspect
  end

  self.per_page = 20

  # set per_page globally
  WillPaginate.per_page = 20

  ##################################################################################################
  ### Includes and Extensions ######################################################################


  ##################################################################################################
  ### Associations #################################################################################


  ##################################################################################################
  ### Callbacks ####################################################################################


  ##################################################################################################
  ### Validations ##################################################################################

  

  ##################################################################################################
  ### Scopes #######################################################################################


  ##################################################################################################
  ### Other ########################################################################################


  ##################################################################################################
  ### Class Methods ################################################################################


  ##################################################################################################
  ### Instance Methods #############################################################################


  #########
  protected
#########


#######
  private
  #######


end
