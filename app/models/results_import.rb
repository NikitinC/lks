class ResultsImport
  include ActiveModel::Model
  require 'roo'

  attr_accessor :file

  def initialize(attributes={})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Неизвестный формат файла: #{file.original_filename}"
    end
  end

  def load_imported_experts
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(5)
    (6..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      expert = Item.find_by_id(row["id"]) || Item.new
      expert.attributes = row.to_hash
      expert
    end
  end

  def imported_results
    @imported_results ||= load_imported_experts
  end


  def importing(file, current_user)
    result = ''
    if 1 == 1
      if !file.nil?
        xlsx = Roo::Spreadsheet.open(file.tempfile)
        subject_last = nil
        subject_id_last = nil
        process_condition_last = nil
        process_condition_id_last = nil
        test_type_last = nil
        test_type_id_last = nil
        if 1 == 1

          # Импорт онлайн-протоколоы
          # ['ID', 'Тип тестирования', 'Предмет', 'Идентификатор', 'Состояние обработки результата', 'Дата тестирования', 'Место тестирования', 'Вариант', 'Первичный балл', 'В 100-балльной шкале', 'В 5-балльной шкале', 'Результат тестовой части', 'Результат заданий с кратким ответом', 'Результат заданий с экспертной оценкой', 'Результат практических (устных) заданий', 'Бланк регистрации', 'Бланк кратких ответов', 'Бланк развёрнутых ответов']
          # ['ID', 'Тип тестирования', 'Предмет', 'Идентификатор', 'Состояние обработки результата', 'Дата',              'Место тестирования', 'Вариант', 'Первичный балл', 'В 100-балльной шкале', 'В 5-балльной шкале', 'Результат тестовой части', 'Результат заданий с кратким ответом', 'Результат заданий с экспертной оценкой', 'Результат практических (устных) заданий', 'Бланк регистрации', 'Бланк кратких ответов', 'Бланк развёрнутых ответов']
          attribute_list = ['-', '-', '-', 'identifier', '-', 'examdate', 'station', 'variant', 'primarymark', 'mark100', 'mark5', 'testresulta', 'testresultb', 'testresultc', 'testresultd', 'blank_r', 'blank_1', 'blank_2']
          if xlsx.sheet(0).row(1) == ['ID', 'Тип тестирования', 'Предмет', 'Идентификатор', 'Состояние обработки результата', 'Дата',         'Место тестирования', 'Вариант', 'Первичный балл', 'В 100-балльной шкале', 'В 5-балльной шкале', 'Результат тестовой части', 'Результат заданий с кратким ответом', 'Результат заданий с экспертной оценкой', 'Результат практических (устных) заданий', 'Бланк регистрации', 'Бланк кратких ответов', 'Бланк развёрнутых ответов']
            i = 1
            while i<xlsx.sheet(0).last_row
              i = i + 1
              xll = xlsx.sheet(0).row(i)
              j = 0
              m = Result.where(:identifier => "#{xll[3]}", :examdate => "#{xll[5]}").first
              sozdali = false
              if m.nil?
                m = Result.new
                sozdali = true
              end
              # puts("string 68")
              if test_type_id_last == xll[1]
                m.test_type_id = test_type_last
              else
                m.test_type_id = TestType.where(:name => xll[1]).first.id
                test_type_last = m.test_type_id
                test_type_id_last = xll[1]
              end
              # puts("string 76")
              if subject_id_last == xll[2]
                m.subject_id = subject_last
              else
                m.subject_id = Subject.where(:subject_name => xll[2]).first.id
                subject_last = m.subject_id
                subject_id_last = xll[2]
              end
              # puts("string 84")
              if process_condition_id_last == xll[4]
                m.process_condition_id = process_condition_last
              else
                m.process_condition_id = ProcessCondition.where(:name => xll[4]).first.id
                process_condition_last = m.process_condition_id
                process_condition_id_last = xll[4]
              end

              # puts("string 92")
              # m.test_type_id = TestType.where(:name => xlsx.sheet(0).row(i)[1]).first.id
              # m.subject_id = Subject.where(:subject_name => xlsx.sheet(0).row(i)[2]).first.id
              # m.process_condition_id = ProcessCondition.where(:name => xlsx.sheet(0).row(i)[4]).first.id

              # m.station = xll[6]
              # m.variant = xll[7]
              # m.primarymark = xll[8]
              # m.mark100 = xll[9]
              # m.mark5 = xll[10]
              # m.testresulta = xll[11]
              # m.testresultb = xll[12]
              # m.testresultc = xll[13]
              # m.testresultd = xll[14]
              # m.blank_r = xll[15]
              # m.blank_1 = xll[16]
              # m.blank_2 = xll[17]
              # puts("string 109")
              # 3 'identifier'
              # 5 'examdate'
              # 6 'station'
              # 7 'variant'
              # 8 'primarymark'
              # 9 'mark100'
              # 10 'mark5'
              # 11 'testresulta'
              # 12 'testresultb'
              # 13 'testresultc'
              # 14 'testresultd'
              # 15 'blank_r'
              # 16 'blank_1'
              # 17 'blank_2'


              while j<attribute_list.count
                if m.has_attribute?(attribute_list[j].to_sym)
                  m.set_property(attribute_list[j], xll[j])
                end
                j = j + 1
              end
              puts("string 132")

              rprew = Result.where(:test_type_id => m.test_type_id, :subject_id => m.subject_id, :identifier => m.identifier).first
              if !rprew.nil?
                if rprew.examdate < m.examdate
                  answers = Answer.where(:test_type_id => m.test_type_id, :subject_id => m.subject_id, :identifier => m.identifier, :examdate => rprew.examdate)
                  result = result + "<tr><td>#{i-1}</td><td>#{xll[2]}</td><td>Найден предыдущий результат, удалён</td></tr>"
                  rprew.destroy
                  answers.each do |a|
                    a.destroy
                  end
                end
              end

              if 2 == 2
                if m.save
                  if sozdali == true
                    result = result + "<tr><td>#{i-1}</td><td>#{xll[2]}</td><td>Удалось, создали</td></tr>"
                  else
                    result = result + "<tr><td>#{i-1}</td><td>#{xll[2]}</td><td>Удалось, обновили</td></tr>"
                  end
                else
                  result = result + "<tr><td>#{i-1}</td><td>#{xll[2]}</td><td>Не удалось: "
                  m.errors.full_messages.each do |message|
                    result = result + "<li>#{message}</li>"
                  end
                  result = result + "</td></tr>"
                end

              else
                result = result + "<tr><td>#{i-1}</td><td>#{xll[2]}</td><td>Не удалось на уровне базы!!: #{m}</td></tr>"
              end

            end

          end
          result = "<table class='table-stripped'><thead><th>Строка №</th><th> - - Запись - -</th><th>Результат загрузки</th>#{result}</table>"
          return result
        end
        return ("Файл не определён")
      end
    else
      return ("Файл вызвал общую ошибку загрузки на уровне базы данных. Сделайте заявку на support с приложением файла.")
    end
  end


  def save
    if imported_results.map(&:valid?).all?
      imported_results.each(&:save!)
      true
    else
      imported_results.each_with_index do |result, index|
        result.errors.full_messages.each do |msg|
          errors.add :base, "Строка #{index + 6}: #{msg}"
        end
      end
      false
    end
  end

end