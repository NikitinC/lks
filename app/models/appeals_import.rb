class AppealsImport
  include ActiveModel::Model
  require 'roo'

  attr_accessor :file

  def initialize(attributes={})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Неизвестный формат файла: #{file.original_filename}"
    end
  end

  def load_imported_experts
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(5)
    (6..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      expert = Item.find_by_id(row["id"]) || Item.new
      expert.attributes = row.to_hash
      expert
    end
  end

  def imported_appeals
    @imported_appeals ||= load_imported_experts
  end

  def save
    if imported_appeals.map(&:valid?).all?
      imported_appeals.each(&:save!)
      true
    else
      imported_appeals.each_with_index do |appeal, index|
        appeal.errors.full_messages.each do |msg|
          errors.add :base, "Строка #{index + 6}: #{msg}"
        end
      end
      false
    end
  end

end