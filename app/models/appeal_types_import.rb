class AppealTypesImport
  include ActiveModel::Model
  require 'roo'

  attr_accessor :file

  def initialize(attributes={})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def load_imported_appeal_types
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(5)
    (6..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      appeal_type = Item.find_by_id(row["id"]) || Item.new
      appeal_type.attributes = row.to_hash
      appeal_type
    end
  end

  def imported_appeal_types
    @imported_appeal_types ||= load_imported_appeal_types
  end

  def save
    if imported_appeal_types.map(&:valid?).all?
      imported_appeal_types.each(&:save!)
      true
    else
      imported_appeal_types.each_with_index do |appeal_type, index|
        appeal_type.errors.full_messages.each do |msg|
          errors.add :base, "Row #{index + 6}: #{msg}"
        end
      end
      false
    end
  end

end