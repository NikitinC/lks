class Subject < ActiveRecord::Base

  has_many :result
  has_many :task_specification
  has_many :user_test_type

  def to_s
    return subject_name + ' [' + code.to_s + ']'
  end

  def name
    return subject_name
  end

  def self.search(search)
    if search
      where('subject_name || cast(code as varchar(5)) LIKE ?', "%#{search}%")
    end
  end

  ##################################################################################################
  ### Includes and Extensions ######################################################################


  ##################################################################################################
  ### Associations #################################################################################


  ##################################################################################################
  ### Callbacks ####################################################################################


  ##################################################################################################
  ### Validations ##################################################################################


  ##################################################################################################
  ### Scopes #######################################################################################


  ##################################################################################################
  ### Other ########################################################################################


  ##################################################################################################
  ### Class Methods ################################################################################


  ##################################################################################################
  ### Instance Methods #############################################################################


  #########
  protected
#########


#######
  private
  #######


end
