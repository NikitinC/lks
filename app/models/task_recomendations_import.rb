class TaskRecomendationsImport
  include ActiveModel::Model
  require 'roo'

  attr_accessor :file

  def initialize(attributes={})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def load_imported_task_recomendations
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(5)
    (6..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      task_recomendation = Item.find_by_id(row["id"]) || Item.new
      task_recomendation.attributes = row.to_hash
      task_recomendation
    end
  end

  def imported_task_recomendations
    @imported_task_recomendations ||= load_imported_task_recomendations
  end

  def save
    if imported_task_recomendations.map(&:valid?).all?
      imported_task_recomendations.each(&:save!)
      true
    else
      imported_task_recomendations.each_with_index do |task_recomendation, index|
        task_recomendation.errors.full_messages.each do |msg|
          errors.add :base, "Row #{index + 6}: #{msg}"
        end
      end
      false
    end
  end

end