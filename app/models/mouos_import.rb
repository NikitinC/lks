class MouosImport
  include ActiveModel::Model
  require 'roo'

  attr_accessor :file

  def initialize(attributes={})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def load_imported_mouos
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(5)
    (6..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      mouo = Item.find_by_id(row["id"]) || Item.new
      mouo.attributes = row.to_hash
      mouo
    end
  end

  def imported_mouos
    @imported_mouos ||= load_imported_mouos
  end

  def save
    if imported_mouos.map(&:valid?).all?
      imported_mouos.each(&:save!)
      true
    else
      imported_mouos.each_with_index do |mouo, index|
        mouo.errors.full_messages.each do |msg|
          errors.add :base, "Row #{index + 6}: #{msg}"
        end
      end
      false
    end
  end

end