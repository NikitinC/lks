class UserTestTypesImport
  include ActiveModel::Model
  require 'roo'

  attr_accessor :file

  def initialize(attributes={})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def load_imported_user_test_types
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(5)
    (6..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      user_test_type = Item.find_by_id(row["id"]) || Item.new
      user_test_type.attributes = row.to_hash
      user_test_type
    end
  end

  def imported_user_test_types
    @imported_user_test_types ||= load_imported_user_test_types
  end

  def importing(file, current_user)
    result = ''
    if 1 == 1
      if !file.nil?
        xlsx = Roo::Spreadsheet.open(file.tempfile)

        if xlsx.sheets.count == 1

          # Импорт онлайн-протоколоы
          attribute_list = ['code', 'name', 'short_name', 'fisgia9code', 'fisgia11code', 'sort_by']
          if xlsx.sheet(0).row(1) == ['Код', 'Наименование', 'Краткое наименование', 'Код в ФИС ГИА-9', 'Код в ФИС ГИА-11', 'Порядок сортировки']
            i = 1
            while i<xlsx.sheet(0).last_row
              i = i + 1
              j = 0
              m = CompositionType.where("cast(code as bigint) = #{xlsx.sheet(0).row(i)[0].to_i}").first
              sozdali = false
              if m.nil?
                m = CompositionType.new
                sozdali = true
              end

              while j<attribute_list.count
                if m.has_attribute?(attribute_list[j].to_sym)
                  m.set_property(attribute_list[j], xlsx.sheet(0).row(i)[j])
                end
                j = j + 1
              end

              begin
                if m.save
                  if sozdali == true
                    result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Удалось, создали</td></tr>"
                  else
                    result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Удалось, обновили</td></tr>"
                  end
                else
                  result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Не удалось: "
                  m.errors.full_messages.each do |message|
                    result = result + "<li>#{message}</li>"
                  end
                  result = result + "</td></tr>"
                end

              rescue
                result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Не удалось на уровне базы!!: #{m}</td></tr>"
              end

            end

          end
          result = "<table class='table-stripped'><thead><th>Строка №</th><th> - - Запись - -</th><th>Результат загрузки</th>#{result}</table>"
          return result
        end
        return ("Файл не определён")
      end
    else
      return ("Файл вызвал общую ошибку загрузки на уровне базы данных. Сделайте заявку на support с приложением файла.")
    end
  end


  def save
    if imported_user_test_types.map(&:valid?).all?
      imported_user_test_types.each(&:save!)
      true
    else
      imported_user_test_types.each_with_index do |user_test_type, index|
        user_test_type.errors.full_messages.each do |msg|
          errors.add :base, "Row #{index + 6}: #{msg}"
        end
      end
      false
    end
  end

end