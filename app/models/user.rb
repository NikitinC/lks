class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable, :registerable
  devise :database_authenticatable,
         # :recoverable, #,
         :rememberable,
         :validatable, #:confirmable
        :masqueradable, :database_authenticatable, #:recoverable, :rememberable,
        :omniauthable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  attr_accessor :allow_blank_password

  # has_one_attached :avatar
  has_person_name

  has_many :notifications, as: :recipient
  has_many :services
  has_many :user_test_type
  has_many :appeal

  # Called by Devise to enable/disable password presence validation
  def password_required?
    allow_blank_password ? false : super
  end

  # Don't require a password when importing users
  def before_import_save(record)
    self.allow_blank_password = true
    self.password = self.last_name
  end

  def has_role? *name
    name.flatten.map(&:to_sym).include? role
  end


  def role
    begin
      self.target_type.underscore.to_sym
    rescue
      false
    end
  end

end
