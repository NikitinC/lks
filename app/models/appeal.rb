class Appeal < ActiveRecord::Base

  belongs_to :appeal_type
  belongs_to :subject
  belongs_to :appeal_condition
  belongs_to :test_type
  belongs_to :appeal_station
  belongs_to :user

  def to_s
    return name
  end

  def self.search(search)
    if search
      where('identifier LIKE ?', "%#{search}%")
    end
  end

  filterrific(
    default_filter_params: { sorted_by: 'id_desc' },
    available_filters: [
      :sorted_by,
      :search_query,
      :appeals_params
    ]
  )

  has_one_attached :notification
  attr_accessor :delete_notification

  has_one_attached :statement
  attr_accessor :delete_statement

  def self.options_for_sorted_by
    [
      ['По дате появления (сначала новые)', 'created_at_desc'],
      ['По дате появления (сначала старые)', 'created_at_asc']
    ]
  end

  self.per_page = 10

  scope :search_query, ->(query) {
    return nil  if query.blank?
    # condition query, parse into individual keywords
    terms = query.downcase.split(/\s+/)
    # replace "*" with "%" for wildcard searches,
    # append '%', remove duplicate '%'s
    terms = terms.map { |e|
      (e.gsub('*', '%') + '%').gsub(/%+/, '%')
    }
    # configure number of OR conditions for provision
    # of interpolation arguments. Adjust this if you
    # change the number of OR conditions.
    num_or_conditions = 2
    where(
      terms.map {
        or_clauses = [
          "LOWER(cast(appeals.id as varchar)) LIKE ?",
          "LOWER(appeals.identifier) LIKE ?",
        ].join(' OR ')
        "(#{ or_clauses })"
      }.join(' AND '),
      *terms.map { |e| [e] * num_or_conditions }.flatten
    )
  }

  scope :sorted_by, ->(sort_option) {
    # extract the sort direction from the param value.
    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
    appeals = Appeal.arel_table
    # appeal_stations = AppealStation.arel_table
    # test_types = TestType.arel_table

    case sort_option.to_s
    when /^created_at_/
      # order("appeals.created_at #{direction}")
      order(appeals[:created_at].send(direction))
    when /^id_/
      # order("appeals.created_at #{direction}")
      order(appeals[:id].send(direction))
    # when /^name_/
      # order("LOWER(appeals.name) #{direction}, LOWER(appeals.name) #{direction}")
      # order(appeals[:name].lower.send(direction)).order(appeals[:name].lower.send(direction))
    # when /^actives/
      # order("LOWER(actives.name) #{ direction }").includes(:actives)
      # Event.joins(:active).order(actives[:name].lower.send(direction))
    # when /^appeal_stations/
      # order("LOWER(districts.name) #{ direction }").includes(:district)
      # Appeal.joins(:appeal_station).order(appeal_stations[:name].lower.send(direction)).order(appeals[:id].lower.send(direction))
    # when /^formats/
      # order("LOWER(districts.name) #{ direction }").includes(:district)
      # Event.joins(:formats).order(formats[:name].lower.send(direction)).order(appeals[:name].lower.send(direction))
    # when /^test_types/
    #   order("LOWER(test_types.name) #{ direction }").includes(:test_type)
    #   Appeal.joins(:test_type).order(test_type[:name].lower.send(direction)).order(appeals[:id].lower.send(direction))
    # when /^examdate/
    #   order("LOWER(appeals.examdate) #{ direction }")
    # when /^duration/
    #   order("LOWER(appeals.duration) #{ direction }")
    # when /^max_participants/
    #   order("LOWER(appeals.max_participants) #{ direction }")
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :appeals_params, -> (appeals_params_attr) {
    appeals = Appeal.all
    if !appeals_params_attr.test_type.nil? and appeals_params_attr.test_type != ''
      appeals = appeals.where(test_type: { id: appeals_params_attr.test_type }).joins(:test_type).distinct
    end
    if !appeals_params_attr.subject.nil? and appeals_params_attr.subject != ''
      appeals = appeals.where(subject: { id: appeals_params_attr.subject }).joins(:subject).distinct
    end
    if !appeals_params_attr.examdate.nil? and appeals_params_attr.examdate != ''
      appeals = appeals.where(:examdate => appeals_params_attr.examdate).distinct
    end
    if !appeals_params_attr.with_errors.nil? and appeals_params_attr.with_errors != ''
      if appeals_params_attr.with_errors == '1'
        appeals = appeals.joins('left join results on upper(results.identifier) = upper(appeals.identifier) and results.examdate = appeals.examdate and results.subject_id = appeals.subject_id').where ('results.id is NULL')
      end
    end
    if !appeals_params_attr.ate.nil? and appeals_params_attr.ate != ''
      appeals = appeals.joins("	join appeal_stations aps ON aps.id = appeals.appeal_station_id join mouos m ON m.id = aps.mouo_id join ates a ON a.id = m.ate_id").where("a.id = " + appeals_params_attr.ate.to_s).distinct
    end
    if !appeals_params_attr.appeal_condition.nil? and appeals_params_attr.appeal_condition != ''
      appeals = appeals.where(appeal_condition: { id: appeals_params_attr.appeal_condition }).joins(:appeal_condition).distinct
    end
    # if !appeals_params_attr.with_created_at_gte.nil? and appeals_params_attr.with_created_at_gte != ''
    #   appeals = appeals.where('appeals.date >= ?', appeals_params_attr.with_created_at_gte).distinct
    # end
    return appeals
  }

  ##################################################################################################
  ### Includes and Extensions ######################################################################


  ##################################################################################################
  ### Associations #################################################################################


  ##################################################################################################
  ### Callbacks ####################################################################################


  ##################################################################################################
  ### Validations ##################################################################################


  ##################################################################################################
  ### Scopes #######################################################################################


  ##################################################################################################
  ### Other ########################################################################################


  ##################################################################################################
  ### Class Methods ################################################################################

  ##################################################################################################
  ### Instance Methods #############################################################################


  #########
  protected
#########


#######
  private
  #######


end
