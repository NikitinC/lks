module RsValidator
  extend ActiveSupport::Concern

  class RsValidator < ActiveModel::EachValidator

    def validate_each(record, attribute, value)
      return if value == nil
      # return if ExceptRecord.all.map(&:name).include? value

      value = value.sub(/И\.\s?о\.\s/, 'Ио ') # Allow I. O.
      value = value.gsub(/\s№\s*[\d]+/, '') # Allow number sign
      value = value.gsub(/\s[А-ЯЁ]\.\s?[А-ЯЁ]\.\s/, ' ') # Allow initials
      # puts value
      value.split.each_with_index do |word, i|
        ok = word =~ /\A["«\(]?[А-ЯЁ]?[а-яё]+(-[А-ЯЁ]?[а-яё]+)?[\.,":»\)]?\z/
        ok &&= word =~ /\A["«]?[А-ЯЁ]/ if i == 0
        ok ||= word == '-'
        ok ||= word =~ /\A[IXV]+\z/
        # print "\t", word, ok.to_s, "\n"
        return record.errors[attribute] << (options[:message] || I18n.t("messages.validation.must_be_a_name")) unless ok
      end
      record.errors[attribute] << (options[:message] || I18n.t("messages.validation.must_be_a_name")) if value =~ /\.\z/
    end
  end


end
