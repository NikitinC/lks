

module PhoneValidator
  extend ActiveSupport::Concern

  class PhoneValidator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
      if !(value =~ /\A\(\d{3}\) \d{7}\z/) && !( value =~ /\A\+7\(\d{3}\)\d{3}-\d{2}\d{2}\z/ )
        record.errors[attribute] << (options[:message] || I18n.t("messages.validation.must_be_a_phone"))
      end
    end
  end


end
