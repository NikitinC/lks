# coding: utf-8

module NamePartValidator
  extend ActiveSupport::Concern

  class NamePartValidator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
      return if ExceptRecord.all.map(&:name).include? value

      value = value.sub(/\s(кизи|угли|гызы|кызы|оглы|огли|кизы|куинь|уулу|аглы|Уулу|Кизи|Кызы|Оглы|Угли)\z/, '')
      unless value =~ /\A[А-ЯЁ][а-яё]+(-[А-ЯЁ]?[а-яё]+)?\z/
        record.errors[attribute] << (options[:message] || I18n.t("messages.validation.must_be_a_name"))
      end
    end
  end

end
