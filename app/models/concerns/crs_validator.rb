module CrsValidator
  extend ActiveSupport::Concern

  class CrsValidator < ActiveModel::EachValidator

    def validate_each(record, attribute, value)
      return if value == nil
      return if ExceptRecord.all.map(&:name).include? value

      value = value.sub(/И\.\s?о\.\s/, 'Ио ')
      value = value.gsub(/\s№\s*[\d]+/, '')
      value = value.gsub(/\s[А-ЯЁ]\.\s?[А-ЯЁ]\.\s/, ' ') # Allow initials without spaces
      # puts value
      value.split.each_with_index do |word, i|
        ok = word =~ /\A["«\(]?[А-ЯЁа-яё]+(-[А-ЯЁа-яё]+)?[\.,":»\)]?\z/
        ok &&= word =~ /\A["«]?[А-ЯЁ]/ if i == 0
        ok ||= word == '-'
        # print "\t", word, ok.to_s, "\n"
        return record.errors[attribute] << (options[:message] || I18n.t("messages.validation.must_be_a_name")) unless ok
      end
      record.errors[attribute] << (options[:message] || I18n.t("messages.validation.must_be_a_name")) if value =~ /\.\z/
    end
  end
end