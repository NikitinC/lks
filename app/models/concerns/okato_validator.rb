module OkatoValidator
  extend ActiveSupport::Concern

  class OkatoValidator < ActiveModel::EachValidator

    def self.calc(okato)
      [1, 3].each do |i|
        code = okato[0..-2].each_with_index.inject(0){ |s, p| s + p[0] * (p[1] + i) } % 11
        return code if code < 10
      end
      0
    end

    def self.compliant?(okato)
      okato = okato.gsub(' ', '')

      return false unless okato =~ /^\d+$/

      okato = okato.split(//).map(&:to_i)

      return false unless [3, 5, 6, 9, 11].include?(okato.size)
      # return false unless calc(okato) == okato.last
      return true
    end

    def validate_each(record, attribute, value)
      unless value.present? && self.class.compliant?(value)
        record.errors.add(attribute, "not_valid_okato")
      end
    end

  end

end
