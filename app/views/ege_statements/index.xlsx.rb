def create_spreadsheet relation
  p = Axlsx::Package.new
  EgeStatement.uncached do
    p.workbook do |wb|
      styles = wb.styles
      title = styles.add_style(:sz => 15, :b => true)
      header = styles.add_style(:b => true, :border => Axlsx::STYLE_THIN_BORDER)
      default = styles.add_style(:border => Axlsx::STYLE_THIN_BORDER)
      wb.add_worksheet(name: I18n.t("spreadsheets.list_ege_statement")) do |sheet|
        sheet.add_row [I18n.t("spreadsheets.list_ege_statement")], :style => title
        sheet.add_row ["#{I18n.t("spreadsheets.export_date")}:#{Russian::l (Time.new)}"]
        sheet.add_row [
                        I18n.t("activerecord.attributes.ege_statement.code"),
                        I18n.t("ege_statements.tables_headers.name"),
                        I18n.t("ege_statements.tables_headers.chief_fio"),
                        I18n.t("ege_statements.tables_headers.chief_phone"),
                        I18n.t("ege_statements.tables_headers.chief_email"),
                        I18n.t("ege_statements.tables_headers.information_exchange_fio"),
                        I18n.t("ege_statements.tables_headers.information_exchange_phone"),
                        I18n.t("ege_statements.tables_headers.information_exchange_email"),
                        I18n.t('ege_statements.tables_headers.specialist_final_certification_full_name'),
                        I18n.t('ege_statements.tables_headers.specialist_final_certification_phone'),
                        I18n.t('ege_statements.tables_headers.specialist_final_certification_email'),
                        I18n.t('ege_statements.tables_headers.exam_materials_specialist_full_name'),
                        I18n.t('ege_statements.tables_headers.exam_materials_specialist_post'),
                        I18n.t("activerecord.attributes.user.login"),
                      ], :style => header
        relation.find_each do |ege_statement|
          sheet.add_row [
                          ege_statement.code,
                          ege_statement.name,
                          ege_statement.chief_full_name,
                          ege_statement.chief_phone,
                          ege_statement.chief_email,
                          ege_statement.information_exchange_specialist_full_name,
                          ege_statement.information_exchange_specialist_phone,
                          ege_statement.information_exchange_specialist_email,
                          ege_statement.specialist_final_certification_full_name,
                          ege_statement.specialist_final_certification_phone,
                          ege_statement.specialist_final_certification_email,
                          ege_statement.exam_materials_specialist_full_name,
                          ege_statement.exam_materials_specialist_post,
                          ege_statement.users.map { |u| u.login }.join(" ,")
                        ], :style => default
        end
        sheet.merge_cells("A1:D1")
      end
    end
  end
  p
end