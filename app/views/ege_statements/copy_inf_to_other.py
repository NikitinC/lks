import tkinter as tk
import shutil
import os
from os import walk

def ensure_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)

subj = ['Русский язык', 'Математика профильная', 'Физика', 'Химия', 'Биология', 'История', 'География', 'Литература', 'Обществознание', 'Английский язык', 'Английский язык (устный)', 'Немецкий язык', 'Немецкий язык (устный)', 'Французский язык', 'Французский язык (устный)', 'Испанский язык', 'Испанский язык (устный)', 'Китайский язык', 'Китайский язык (устный)']
nnames = ['rus', 'math', 'phys', 'chem', 'bio', 'his', 'geo', 'lit', 'soc', 'eng', 'engu', 'deu', 'deuu', 'fra', 'frau', 'isp', 'ispu', 'kit', 'kitu']

oldmodel_name = 'inf'
oldmodel_name_big = 'Информатика и ИКТ'

for i in range(19):

    newmodel_name = nnames[i]
    newmodel_name_big = subj[i]

    print("Creating:", newmodel_name)

    with open('_list_inf.html.haml', 'r') as file :
        filedata = file.read()

        # Replace the target string
        filedata = filedata.replace(oldmodel_name, newmodel_name)
        filedata = filedata.replace(oldmodel_name_big, newmodel_name_big)
        outname = '_list_' + newmodel_name + '.html.haml'
        # Write the file out again
        with open(outname, 'w') as file:
           file.write(filedata)
