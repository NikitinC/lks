def create_spreadsheet relation
  p = Axlsx::Package.new
  Exam.uncached do
    p.workbook do |wb|
      styles = wb.styles
      title = styles.add_style(:sz => 15, :b => true)
      header = styles.add_style(:b => true, :border => Axlsx::STYLE_THIN_BORDER)
      default = styles.add_style(:border => Axlsx::STYLE_THIN_BORDER)
      wb.add_worksheet(name: I18n.t("spreadsheets.list_exam")) do |sheet|
        sheet.add_row [I18n.t("spreadsheets.list_exam")], :style => title
        sheet.add_row ["#{I18n.t("spreadsheets.export_date")}:#{Russian::l (Time.new)}"]
        sheet.add_row [
                          I18n.t("activerecord.attributes.exam.code"),
                          I18n.t("exams.tables_headers.name"),
                          I18n.t("exams.tables_headers.chief_fio"),
                          I18n.t("exams.tables_headers.chief_phone"),
                          I18n.t("exams.tables_headers.chief_email"),
                          I18n.t("exams.tables_headers.information_exchange_fio"),
                          I18n.t("exams.tables_headers.information_exchange_phone"),
                          I18n.t("exams.tables_headers.information_exchange_email"),
                          I18n.t('exams.tables_headers.specialist_final_certification_full_name'),
                          I18n.t('exams.tables_headers.specialist_final_certification_phone'),
                          I18n.t('exams.tables_headers.specialist_final_certification_email'),
                          I18n.t('exams.tables_headers.exam_materials_specialist_full_name'),
                          I18n.t('exams.tables_headers.exam_materials_specialist_post'),
                          I18n.t("activerecord.attributes.user.login"),
                      ], :style => header
        relation.find_each do |exam|
          sheet.add_row [
                            exam.code,
                            exam.name,
                            exam.chief_full_name,
                            exam.chief_phone,
                            exam.chief_email,
                            exam.information_exchange_specialist_full_name,
                            exam.information_exchange_specialist_phone,
                            exam.information_exchange_specialist_email,
                            exam.specialist_final_certification_full_name,
                            exam.specialist_final_certification_phone,
                            exam.specialist_final_certification_email,
                            exam.exam_materials_specialist_full_name,
                            exam.exam_materials_specialist_post,
                            exam.users.map { |u| u.login }.join(" ,")
                        ], :style => default
        end
        sheet.merge_cells("A1:D1")
      end
    end
  end
  p
end