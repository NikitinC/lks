def create_spreadsheet relation
  p = Axlsx::Package.new
  Citizenship.uncached do
    p.workbook do |wb|
      styles = wb.styles
      title = styles.add_style(:sz => 15, :b => true)
      header = styles.add_style(:b => true, :border => Axlsx::STYLE_THIN_BORDER)
      default = styles.add_style(:border => Axlsx::STYLE_THIN_BORDER)
      wb.add_worksheet(name: I18n.t("spreadsheets.list_citizenship")) do |sheet|
        sheet.add_row [I18n.t("spreadsheets.list_citizenship")], :style => title
        sheet.add_row ["#{I18n.t("spreadsheets.export_date")}:#{Russian::l (Time.new)}"]
        sheet.add_row [
                          I18n.t("activerecord.attributes.citizenship.code"),
                          I18n.t("citizenships.tables_headers.name"),
                          I18n.t("citizenships.tables_headers.chief_fio"),
                          I18n.t("citizenships.tables_headers.chief_phone"),
                          I18n.t("citizenships.tables_headers.chief_email"),
                          I18n.t("citizenships.tables_headers.information_exchange_fio"),
                          I18n.t("citizenships.tables_headers.information_exchange_phone"),
                          I18n.t("citizenships.tables_headers.information_exchange_email"),
                          I18n.t('citizenships.tables_headers.specialist_final_certification_full_name'),
                          I18n.t('citizenships.tables_headers.specialist_final_certification_phone'),
                          I18n.t('citizenships.tables_headers.specialist_final_certification_email'),
                          I18n.t('citizenships.tables_headers.exam_materials_specialist_full_name'),
                          I18n.t('citizenships.tables_headers.exam_materials_specialist_post'),
                          I18n.t("activerecord.attributes.user.login"),
                      ], :style => header
        relation.find_each do |citizenship|
          sheet.add_row [
                            citizenship.code,
                            citizenship.name,
                            citizenship.chief_full_name,
                            citizenship.chief_phone,
                            citizenship.chief_email,
                            citizenship.information_exchange_specialist_full_name,
                            citizenship.information_exchange_specialist_phone,
                            citizenship.information_exchange_specialist_email,
                            citizenship.specialist_final_certification_full_name,
                            citizenship.specialist_final_certification_phone,
                            citizenship.specialist_final_certification_email,
                            citizenship.exam_materials_specialist_full_name,
                            citizenship.exam_materials_specialist_post,
                            citizenship.users.map { |u| u.login }.join(" ,")
                        ], :style => default
        end
        sheet.merge_cells("A1:D1")
      end
    end
  end
  p
end