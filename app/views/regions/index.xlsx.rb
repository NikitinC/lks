def create_spreadsheet relation
  p = Axlsx::Package.new
  Region.uncached do
    p.workbook do |wb|
      styles = wb.styles
      title = styles.add_style(:sz => 15, :b => true)
      header = styles.add_style(:b => true, :border => Axlsx::STYLE_THIN_BORDER)
      default = styles.add_style(:border => Axlsx::STYLE_THIN_BORDER)
      wb.add_worksheet(name: I18n.t("spreadsheets.list_region")) do |sheet|
        sheet.add_row [I18n.t("spreadsheets.list_region")], :style => title
        sheet.add_row ["#{I18n.t("spreadsheets.export_date")}:#{Russian::l (Time.new)}"]
        sheet.add_row [
                          I18n.t("activerecord.attributes.region.code"),
                          I18n.t("regions.tables_headers.name"),
                          I18n.t("regions.tables_headers.chief_fio"),
                          I18n.t("regions.tables_headers.chief_phone"),
                          I18n.t("regions.tables_headers.chief_email"),
                          I18n.t("regions.tables_headers.information_exchange_fio"),
                          I18n.t("regions.tables_headers.information_exchange_phone"),
                          I18n.t("regions.tables_headers.information_exchange_email"),
                          I18n.t('regions.tables_headers.specialist_final_certification_full_name'),
                          I18n.t('regions.tables_headers.specialist_final_certification_phone'),
                          I18n.t('regions.tables_headers.specialist_final_certification_email'),
                          I18n.t('regions.tables_headers.exam_materials_specialist_full_name'),
                          I18n.t('regions.tables_headers.exam_materials_specialist_post'),
                          I18n.t("activerecord.attributes.user.login"),
                      ], :style => header
        relation.find_each do |region|
          sheet.add_row [
                            region.code,
                            region.name,
                            region.chief_full_name,
                            region.chief_phone,
                            region.chief_email,
                            region.information_exchange_specialist_full_name,
                            region.information_exchange_specialist_phone,
                            region.information_exchange_specialist_email,
                            region.specialist_final_certification_full_name,
                            region.specialist_final_certification_phone,
                            region.specialist_final_certification_email,
                            region.exam_materials_specialist_full_name,
                            region.exam_materials_specialist_post,
                            region.users.map { |u| u.login }.join(" ,")
                        ], :style => default
        end
        sheet.merge_cells("A1:D1")
      end
    end
  end
  p
end