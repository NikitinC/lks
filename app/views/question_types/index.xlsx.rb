def create_spreadsheet relation
  p = Axlsx::Package.new
  QuestionType.uncached do
    p.workbook do |wb|
      styles = wb.styles
      title = styles.add_style(:sz => 15, :b => true)
      header = styles.add_style(:b => true, :border => Axlsx::STYLE_THIN_BORDER)
      default = styles.add_style(:border => Axlsx::STYLE_THIN_BORDER)
      wb.add_worksheet(name: I18n.t("spreadsheets.list_question_type")) do |sheet|
        sheet.add_row [I18n.t("spreadsheets.list_question_type")], :style => title
        sheet.add_row ["#{I18n.t("spreadsheets.export_date")}:#{Russian::l (Time.new)}"]
        sheet.add_row [
                          I18n.t("activerecord.attributes.question_type.code"),
                          I18n.t("question_types.tables_headers.name"),
                          I18n.t("question_types.tables_headers.chief_fio"),
                          I18n.t("question_types.tables_headers.chief_phone"),
                          I18n.t("question_types.tables_headers.chief_email"),
                          I18n.t("question_types.tables_headers.information_exchange_fio"),
                          I18n.t("question_types.tables_headers.information_exchange_phone"),
                          I18n.t("question_types.tables_headers.information_exchange_email"),
                          I18n.t('question_types.tables_headers.specialist_final_certification_full_name'),
                          I18n.t('question_types.tables_headers.specialist_final_certification_phone'),
                          I18n.t('question_types.tables_headers.specialist_final_certification_email'),
                          I18n.t('question_types.tables_headers.exam_materials_specialist_full_name'),
                          I18n.t('question_types.tables_headers.exam_materials_specialist_post'),
                          I18n.t("activerecord.attributes.user.login"),
                      ], :style => header
        relation.find_each do |question_type|
          sheet.add_row [
                            question_type.code,
                            question_type.name,
                            question_type.chief_full_name,
                            question_type.chief_phone,
                            question_type.chief_email,
                            question_type.information_exchange_specialist_full_name,
                            question_type.information_exchange_specialist_phone,
                            question_type.information_exchange_specialist_email,
                            question_type.specialist_final_certification_full_name,
                            question_type.specialist_final_certification_phone,
                            question_type.specialist_final_certification_email,
                            question_type.exam_materials_specialist_full_name,
                            question_type.exam_materials_specialist_post,
                            question_type.users.map { |u| u.login }.join(" ,")
                        ], :style => default
        end
        sheet.merge_cells("A1:D1")
      end
    end
  end
  p
end