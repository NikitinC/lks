def create_spreadsheet relation
  p = Axlsx::Package.new
  RegistrationPlace.uncached do
    p.workbook do |wb|
      styles = wb.styles
      title = styles.add_style(:sz => 15, :b => true)
      header = styles.add_style(:b => true, :border => Axlsx::STYLE_THIN_BORDER)
      default = styles.add_style(:border => Axlsx::STYLE_THIN_BORDER)
      wb.add_worksheet(name: I18n.t("spreadsheets.list_registration_place")) do |sheet|
        sheet.add_row [I18n.t("spreadsheets.list_registration_place")], :style => title
        sheet.add_row ["#{I18n.t("spreadsheets.export_date")}:#{Russian::l (Time.new)}"]
        sheet.add_row [
                          I18n.t("activerecord.attributes.registration_place.code"),
                          I18n.t("registration_places.tables_headers.name"),
                          I18n.t("registration_places.tables_headers.chief_fio"),
                          I18n.t("registration_places.tables_headers.chief_phone"),
                          I18n.t("registration_places.tables_headers.chief_email"),
                          I18n.t("registration_places.tables_headers.information_exchange_fio"),
                          I18n.t("registration_places.tables_headers.information_exchange_phone"),
                          I18n.t("registration_places.tables_headers.information_exchange_email"),
                          I18n.t('registration_places.tables_headers.specialist_final_certification_full_name'),
                          I18n.t('registration_places.tables_headers.specialist_final_certification_phone'),
                          I18n.t('registration_places.tables_headers.specialist_final_certification_email'),
                          I18n.t('registration_places.tables_headers.exam_materials_specialist_full_name'),
                          I18n.t('registration_places.tables_headers.exam_materials_specialist_post'),
                          I18n.t("activerecord.attributes.user.login"),
                      ], :style => header
        relation.find_each do |registration_place|
          sheet.add_row [
                            registration_place.code,
                            registration_place.name,
                            registration_place.chief_full_name,
                            registration_place.chief_phone,
                            registration_place.chief_email,
                            registration_place.information_exchange_specialist_full_name,
                            registration_place.information_exchange_specialist_phone,
                            registration_place.information_exchange_specialist_email,
                            registration_place.specialist_final_certification_full_name,
                            registration_place.specialist_final_certification_phone,
                            registration_place.specialist_final_certification_email,
                            registration_place.exam_materials_specialist_full_name,
                            registration_place.exam_materials_specialist_post,
                            registration_place.users.map { |u| u.login }.join(" ,")
                        ], :style => default
        end
        sheet.merge_cells("A1:D1")
      end
    end
  end
  p
end