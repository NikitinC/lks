json.extract! task_recomendation, :id, :recomendation_text, :test_type_id, :subject_id, :task_number
json.url task_recomendation_url(task_recomendation, format: :json)
