def create_spreadsheet relation
  p = Axlsx::Package.new
  DocumentType.uncached do
    p.workbook do |wb|
      styles = wb.styles
      title = styles.add_style(:sz => 15, :b => true)
      header = styles.add_style(:b => true, :border => Axlsx::STYLE_THIN_BORDER)
      default = styles.add_style(:border => Axlsx::STYLE_THIN_BORDER)
      wb.add_worksheet(name: I18n.t("spreadsheets.list_document_type")) do |sheet|
        sheet.add_row [I18n.t("spreadsheets.list_document_type")], :style => title
        sheet.add_row ["#{I18n.t("spreadsheets.export_date")}:#{Russian::l (Time.new)}"]
        sheet.add_row [
                          I18n.t("activerecord.attributes.document_type.code"),
                          I18n.t("document_types.tables_headers.name"),
                          I18n.t("document_types.tables_headers.chief_fio"),
                          I18n.t("document_types.tables_headers.chief_phone"),
                          I18n.t("document_types.tables_headers.chief_email"),
                          I18n.t("document_types.tables_headers.information_exchange_fio"),
                          I18n.t("document_types.tables_headers.information_exchange_phone"),
                          I18n.t("document_types.tables_headers.information_exchange_email"),
                          I18n.t('document_types.tables_headers.specialist_final_certification_full_name'),
                          I18n.t('document_types.tables_headers.specialist_final_certification_phone'),
                          I18n.t('document_types.tables_headers.specialist_final_certification_email'),
                          I18n.t('document_types.tables_headers.exam_materials_specialist_full_name'),
                          I18n.t('document_types.tables_headers.exam_materials_specialist_post'),
                          I18n.t("activerecord.attributes.user.login"),
                      ], :style => header
        relation.find_each do |document_type|
          sheet.add_row [
                            document_type.code,
                            document_type.name,
                            document_type.chief_full_name,
                            document_type.chief_phone,
                            document_type.chief_email,
                            document_type.information_exchange_specialist_full_name,
                            document_type.information_exchange_specialist_phone,
                            document_type.information_exchange_specialist_email,
                            document_type.specialist_final_certification_full_name,
                            document_type.specialist_final_certification_phone,
                            document_type.specialist_final_certification_email,
                            document_type.exam_materials_specialist_full_name,
                            document_type.exam_materials_specialist_post,
                            document_type.users.map { |u| u.login }.join(" ,")
                        ], :style => default
        end
        sheet.merge_cells("A1:D1")
      end
    end
  end
  p
end