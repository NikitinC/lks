def create_spreadsheet relation
  p = Axlsx::Package.new
  AnswerType.uncached do
    p.workbook do |wb|
      styles = wb.styles
      title = styles.add_style(:sz => 15, :b => true)
      header = styles.add_style(:b => true, :border => Axlsx::STYLE_THIN_BORDER)
      default = styles.add_style(:border => Axlsx::STYLE_THIN_BORDER)
      wb.add_worksheet(name: I18n.t("spreadsheets.list_answer_type")) do |sheet|
        sheet.add_row [I18n.t("spreadsheets.list_answer_type")], :style => title
        sheet.add_row ["#{I18n.t("spreadsheets.export_date")}:#{Russian::l (Time.new)}"]
        sheet.add_row [
                          I18n.t("activerecord.attributes.answer_type.code"),
                          I18n.t("answer_types.tables_headers.name"),
                          I18n.t("answer_types.tables_headers.chief_fio"),
                          I18n.t("answer_types.tables_headers.chief_phone"),
                          I18n.t("answer_types.tables_headers.chief_email"),
                          I18n.t("answer_types.tables_headers.information_exchange_fio"),
                          I18n.t("answer_types.tables_headers.information_exchange_phone"),
                          I18n.t("answer_types.tables_headers.information_exchange_email"),
                          I18n.t('answer_types.tables_headers.specialist_final_certification_full_name'),
                          I18n.t('answer_types.tables_headers.specialist_final_certification_phone'),
                          I18n.t('answer_types.tables_headers.specialist_final_certification_email'),
                          I18n.t('answer_types.tables_headers.exam_materials_specialist_full_name'),
                          I18n.t('answer_types.tables_headers.exam_materials_specialist_post'),
                          I18n.t("activerecord.attributes.user.login"),
                      ], :style => header
        relation.find_each do |answer_type|
          sheet.add_row [
                            answer_type.code,
                            answer_type.name,
                            answer_type.chief_full_name,
                            answer_type.chief_phone,
                            answer_type.chief_email,
                            answer_type.information_exchange_specialist_full_name,
                            answer_type.information_exchange_specialist_phone,
                            answer_type.information_exchange_specialist_email,
                            answer_type.specialist_final_certification_full_name,
                            answer_type.specialist_final_certification_phone,
                            answer_type.specialist_final_certification_email,
                            answer_type.exam_materials_specialist_full_name,
                            answer_type.exam_materials_specialist_post,
                            answer_type.users.map { |u| u.login }.join(" ,")
                        ], :style => default
        end
        sheet.merge_cells("A1:D1")
      end
    end
  end
  p
end