class RegionsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @regions_import = RegionsImport.new
  end

  def create
    @regions_import = RegionsImport.new(params[:regions_import])
    render :create
  end

end
