class AppealStationsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def new
    @appeal_stations_import = AppealStationsImport.new
  end

  def create
    @appeal_stations_import = AppealStationsImport.new(params[:appeal_stations_import])
    render :create
  end

end
