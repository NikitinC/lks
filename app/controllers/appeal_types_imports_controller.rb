class AppealTypesImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def new
    @appeal_types_import = AppealTypesImport.new
  end

  def create
    @appeal_types_import = AppealTypesImport.new(params[:appeal_types_import])
    render :create
  end

end
