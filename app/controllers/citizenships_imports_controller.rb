class CitizenshipsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @citizenships_import = CitizenshipsImport.new
  end

  def create
    @citizenships_import = CitizenshipsImport.new(params[:citizenships_import])
    render :create
  end

end
