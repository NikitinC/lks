class QuestionTypesController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @question_types = QuestionType.accessible_by(current_ability).paginate(page: params[:page]).order('created_at DESC')
    if current_user.admin?
      if !params[:search].nil?
        @question_types = QuestionType.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('created_at DESC')
      end
    else
      if !params[:search].nil?
        @question_types = QuestionType.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('created_at DESC')
      end
    end
  end

  def export
    @question_types = QuestionType.accessible_by(current_ability).order('created_at DESC')
    render xlsx: 'question_types_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/question_types/export.xlsx.axlsx'
    # render "export.xlsx.axlsx"
  end

  def new
    @question_type = QuestionType.new
    if !@question_type.nil?
      render :edit
    else
      flash[:success] = t("cctvs.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def show
    @question_type = QuestionType.accessible_by(current_ability).find(params[:id])
    @previous = QuestionType.accessible_by(current_ability).where("id < " + @question_type.id.to_s).order("id DESC").first
    @next = QuestionType.accessible_by(current_ability).where("id > " + @question_type.id.to_s).order("id ASC").first
    @question_types = QuestionType.accessible_by(current_ability).all.order('created_at DESC')
    if can? :view, @question_type
      render :action => 'show'
    end
  end

  def create
    @question_type = QuestionType.new(question_type_params)
    if @question_type.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @question_type = QuestionType.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @question_type }
    end
  end

  def modaledit
    @question_type = QuestionType.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @question_type }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    QuestionType.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @question_type = QuestionType.accessible_by(current_ability).find(params[:id])
    @question_type.destroy
    respond_to do |format|
      format.html { redirect_to question_types_url, notice: t("question_types.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @question_type = QuestionType.accessible_by(current_ability).find(params[:id])

    if @question_type.update question_type_params
      respond_to do |format|
        format.html {
          flash[:success] = t("question_types.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @question_type.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("question_types." + er.attribute.to_s) + ': ' + t("question_types.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@question_type) }
      end
    end

  end

  def question_type_params
    params.require(:question_type).permit(:name, :deleted_at, :created_at, :updated_at)
  end

  private
  def undo_link
    view_context.link_to("undo", revert_version_path(@question_type.versions.scoped.last), :method => :post)
  end

end
