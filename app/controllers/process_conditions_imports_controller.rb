class ProcessConditionsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def new
    @process_conditions_import = ProcessConditionsImport.new
  end

  def create
    @process_conditions_import = ProcessConditionsImport.new(params[:process_conditions_import])
    render :create
  end

end
