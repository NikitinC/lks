class OusImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def new
    @ous_import = OusImport.new
  end

  def create
    @ous_import = OusImport.new(params[:ous_import])
    render :create
  end

end
