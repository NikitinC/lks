class TaskRecomendationsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @test_types=TestType.all
    @subjects=Subject.all
    @task_recomendations = TaskRecomendation.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if !params[:search].nil?
      @task_recomendations = TaskRecomendation.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
    end
  end

  def export
    @task_recomendations = TaskRecomendation.accessible_by(current_ability).order('id ASC')
    render xlsx: 'task_recomendations_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/task_recomendations/export.xlsx.axlsx'
  end


  def new
    @test_types=TestType.all
    @subjects=Subject.all
    @task_recomendation = TaskRecomendation.new
  end

  def show
    @task_recomendation = TaskRecomendation.find(params[:id])
  end

  def create
    @test_types=TestType.all
    @subjects=Subject.all
    @task_recomendation = TaskRecomendation.new(task_recomendation_params)
    if @task_recomendation.save
      format.html {redirect_to :action => 'index', notice: 'Task recomendation was successfully created.'}
      format.json { render :show, status: :created, location: @task_recomendation }
    else
      # This line overrides the default rendering behavior, which
      # would have been to render the "create" view.
      format.html { render :action => 'new' }
      format.json { render json: @task_recomendation.errors, status: :unprocessable_entity }
    end
  end

  def edit
    @test_types=TestType.all
    @subjects=Subject.all
    @task_recomendation = TaskRecomendation.find(params[:id])
  end

  def delete
    TaskRecomendation.find(params[:id]).destroy
    redirect_to :action => 'index'
  end

  def destroy
    @task_recomendation = TaskRecomendation.find(params[:id])
    @task_recomendation.destroy
    respond_to do |format|
      format.html { redirect_to task_recomendations_url, notice: t("task_recomendations.notice.destroyed") }
      format.json { head :no_content }
    end
  end


  def update
    @task_recomendation = TaskRecomendation.find params[:id]
    respond_to do |format|
      if @task_recomendation.update (task_recomendation_params)
        format.html { redirect_to @task_recomendation, notice: t("task_recomendations.notice.updated") }
        format.json { render :show, status: :ok, location: @task_recomendation }
      else
        format.html { render :edit }
        format.json { render json: @task_recomendation.errors, status: :unprocessable_entity }
      end
    end
  end

  def task_recomendation_params
    params.require(:task_recomendation).permit(:test_type_id, :subject_id, :task_number, :recomendation_text)
  end

end