class ExamWavesImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @exam_waves_import = ExamWavesImport.new
  end

  def create
    @exam_waves_import = ExamWavesImport.new(params[:exam_waves_import])
    render :create
  end

end
