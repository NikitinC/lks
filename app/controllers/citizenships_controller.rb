class CitizenshipsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @citizenships = Citizenship.accessible_by(current_ability).paginate(page: params[:page]).order('code ASC')
    if current_user.admin?
      if !params[:search].nil?
        @citizenships = Citizenship.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    else
      if !params[:search].nil?
        @citizenships = Citizenship.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    end
  end

  def export
    @citizenships = Citizenship.accessible_by(current_ability).order('code ASC')
    render xlsx: 'citizenships_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/citizenships/export.xlsx.axlsx'
    # render "export.xlsx.axlsx"
  end

  def new
    @citizenship = Citizenship.new
    if !@citizenship.nil?
       render :edit
    else
       flash[:success] = t("citizenships.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @citizenship = Citizenship.accessible_by(current_ability).find(params[:id])
    @previous = Citizenship.accessible_by(current_ability).where("id < " + @citizenship.id.to_s).order("id DESC").first
    @next = Citizenship.accessible_by(current_ability).where("id > " + @citizenship.id.to_s).order("id ASC").first
    @citizenships = Citizenship.accessible_by(current_ability).all.order('code ASC')
    if can? :view, @citizenship
      render :action => 'show'
    end
  end

  def create
    @citizenship = Citizenship.new(citizenship_params)
    if @citizenship.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @citizenship = Citizenship.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @citizenship }
    end
  end

  def modaledit
    @citizenship = Citizenship.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @citizenship }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    Citizenship.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @citizenship = Citizenship.accessible_by(current_ability).find(params[:id])
    @citizenship.destroy
    respond_to do |format|
      format.html { redirect_to citizenships_url, notice: t("citizenships.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @citizenship = Citizenship.accessible_by(current_ability).find(params[:id])

    if @citizenship.update citizenship_params
      respond_to do |format|
        format.html {
          flash[:success] = t("citizenships.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @citizenship.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("citizenships." + er.attribute.to_s) + ': ' + t("citizenships.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@citizenship) }
      end
    end

  end

  def citizenship_params
    params.require(:citizenship).permit(:code, :name, :created_at, :updated_at, :short_name, :fisgia9code, :fisgia11code, :sort_by)
  end

  private
  def undo_link
    view_context.link_to("undo", revert_version_path(@citizenship.versions.scoped.last), :method => :post)
  end

end
