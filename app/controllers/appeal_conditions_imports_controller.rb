class AppealConditionsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def new
    @appeal_conditions_import = AppealConditionsImport.new
  end

  def create
    @appeal_conditions_import = AppealConditionsImport.new(params[:appeal_conditions_import])
    render :create
  end

end
