class AppealConditionsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @appeal_conditions = AppealCondition.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if !params[:search].nil?
      @appeal_conditions = AppealCondition.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
    end
  end

  def export
    @appeal_conditions = AppealCondition.accessible_by(current_ability).order('id ASC')
    render xlsx: 'appeal_conditions_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/appeal_conditions/export.xlsx.axlsx'
  end


  def new
    @appeal_condition = AppealCondition.new
  end

  def show
    @appeal_condition = AppealCondition.find(params[:id])
  end

  def create
    @appeal_condition = AppealCondition.new(appeal_condition_params)
    if @appeal_condition.save
      redirect_to :action => 'index'
    else
      # This line overrides the default rendering behavior, which
      # would have been to render the "create" view.
      render :action => 'new'
    end
  end

  def edit
    @appeal_condition = AppealCondition.find(params[:id])
  end

  def delete
    AppealCondition.find(params[:id]).destroy
    redirect_to :action => 'index'
  end

  def destroy
    @appeal_condition = AppealCondition.find(params[:id])
    @appeal_condition.destroy
    respond_to do |format|
      format.html { redirect_to appeal_conditions_url, notice: t("appeal_conditions.notice.destroyed") }
      format.json { head :no_content }
    end
  end


  def update
    @appeal_condition = AppealCondition.find params[:id]
    # respond_to do |format|
    if @appeal_condition.update appeal_condition_params
      flash[:success] = t("appeal_conditions.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def appeal_condition_params
    params.require(:appeal_condition).permit(:code, :name)
  end

end
