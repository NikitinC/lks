class MouosController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @mouos = Mouo.accessible_by(current_ability).paginate(page: params[:page]).order('code ASC')
    if current_user.admin?
      if !params[:search].nil?
        @mouos = Mouo.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    else
      if !params[:search].nil?
        @mouos = Mouo.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    end
  end

  def export
    @mouos = Mouo.accessible_by(current_ability)
    render xlsx: 'mouos_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/mouos/export.xlsx.axlsx'
  end

  def new
    @mouo = Mouo.new
    @ates = Ate.accessible_by(current_ability)
    if !@mouo.nil?
      render :edit
    else
      flash[:success] = t("mouos.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def show
    @mouo = Mouo.accessible_by(current_ability).find(params[:id])
  end

  def create
    @ates = Ate.accessible_by(current_ability)
    @mouo = Mouo.new(mouo_params)
    if @mouo.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @ates = Ate.accessible_by(current_ability)
    @mouo = Mouo.accessible_by(current_ability).find(params[:id])
  end

  def delete
    Mouo.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @mouo = Mouo.accessible_by(current_ability).find(params[:id])
    @mouo.destroy
    respond_to do |format|
      format.html { redirect_to mouos_url, notice: t("mouos.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @mouo = Mouo.accessible_by(current_ability).find params[:id]
    # respond_to do |format|
    if @mouo.update mouo_params
      flash[:success] = t("mouos.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end


  def mouo_params
    params.require(:mouo).permit(:code, :name, :ate_id, :contact_fio, :contact_phone)
  end


end
