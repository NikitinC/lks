class ShowBlanksController < ApplicationController
  before_action :authenticate_user!

  def index

    (@filterrific = initialize_filterrific(
      Result,
      params[:filterrific],
      select_options: {
        sorted_by: Result.options_for_sorted_by,
      },
      )) || return
    @results = @filterrific.find.page(params[:page])
    @subjects = Subject.all
    @test_types = TestType.all
    respond_to do |format|
      format.html
      format.js
    end
  rescue ActiveRecord::RecordNotFound => e
    puts "Had to reset filterrific params: #{ e.message }"
    redirect_to(reset_filterrific_url(format: :html)) and return
  end

end
