class AtesController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @ates = Ate.accessible_by(current_ability).paginate(page: params[:page]).order('code ASC')
    if current_user.admin?
      if !params[:search].nil?
        @ates = Ate.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    else
      if !params[:search].nil?
        @ates = Ate.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    end
  end

  def export
    @ates = Ate.accessible_by(current_ability)
    render xlsx: 'ates_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/ates/export.xlsx.axlsx'
    # render "export.xlsx.axlsx"
  end

  def new
    @ate = Ate.new
    if !@ate.nil?
       render :edit
    else
       flash[:success] = t("ates.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @ate = Ate.accessible_by(current_ability).find(params[:id])
  end

  def create
    @ate = Ate.new(ate_params)
    if @ate.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @ate = Ate.accessible_by(current_ability).find(params[:id])
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    Ate.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @ate = Ate.accessible_by(current_ability).find(params[:id])
    @ate.destroy
    respond_to do |format|
      format.html { redirect_to ates_url, notice: t("ates.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @ate = Ate.accessible_by(current_ability).find params[:id]
    # respond_to do |format|
    if @ate.update ate_params
      flash[:success] = t("ates.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end


  def ate_params
    params.require(:ate).permit(:code, :name)
  end

end
