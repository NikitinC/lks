class AppealStationsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @appeal_stations = AppealStation.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if !params[:search].nil?
      @appeal_stations = AppealStation.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
    end
  end

  def export
    @appeal_stations = AppealStation.accessible_by(current_ability).order('id ASC')
    render xlsx: 'appeal_stations_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/appeal_stations/export.xlsx.axlsx'
  end


  def new
    @appeal_station = AppealStation.new
  end

  def show
    @appeal_station = AppealStation.find(params[:id])
  end

  def create
    @appeal_station = AppealStation.new(appeal_station_params)
    if @appeal_station.save
      redirect_to :action => 'index'
    else
      # This line overrides the default rendering behavior, which
      # would have been to render the "create" view.
      render :action => 'new'
    end
  end

  def edit
    @appeal_station = AppealStation.find(params[:id])
  end

  def delete
    AppealStation.find(params[:id]).destroy
    redirect_to :action => 'index'
  end

  def destroy
    @appeal_station = AppealStation.find(params[:id])
    @appeal_station.destroy
    respond_to do |format|
      format.html { redirect_to appeal_stations_url, notice: t("appeal_stations.notice.destroyed") }
      format.json { head :no_content }
    end
  end


  def update
    @appeal_station = AppealStation.find params[:id]
    # respond_to do |format|
    if @appeal_station.update appeal_station_params
      flash[:success] = t("appeal_stations.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def appeal_station_params
    params.require(:appeal_station).permit(:mouo_id, :address, :name)
  end

end
