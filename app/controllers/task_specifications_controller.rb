class TaskSpecificationsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @test_types=TestType.all
    @subjects=Subject.all
    @task_specifications = TaskSpecification.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if !params[:search].nil?
      @task_specifications = TaskSpecification.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
    end
  end

  def export
    @task_specifications = TaskSpecification.accessible_by(current_ability).order('id ASC')
    render xlsx: 'task_specifications_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/task_specification/export.xlsx.axlsx'
  end


  def new
    @test_types=TestType.all
    @subjects=Subject.all
    @task_specification = TaskSpecification.new
  end

  def show
    @task_specification = TaskSpecification.find(params[:id])
  end

  def create
    @test_types=TestType.all
    @subjects=Subject.all
    @task_specification = TaskSpecification.new(task_specifications_params)
    if @task_specification.save
      redirect_to :action => 'index'
    else
      # This line overrides the default rendering behavior, which
      # would have been to render the "create" view.
      render :action => 'new'
    end
  end

  def edit
    @test_types=TestType.all
    @subjects=Subject.all
    @task_specification = TaskSpecification.find(params[:id])
  end

  def delete
    TaskSpecification.find(params[:id]).destroy
    redirect_to :action => 'index'
  end

  def destroy
    @task_specification = TaskSpecification.find(params[:id])
    @task_specification.destroy
    respond_to do |format|
      format.html { redirect_to task_specifications_url, notice: t("task_specification.notice.destroyed") }
      format.json { head :no_content }
    end
  end


  def update
    @task_specification = TaskSpecification.find params[:id]
    # respond_to do |format|
    if @task_specification.update task_specifications_params
      flash[:success] = t("task_specification.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def task_specifications_params
    params.require(:task_specification).permit(:test_type_id, :subject_id, :task_number, :competention_type, :competention_name, :zun_name, :max_ball)
  end

end