class EgeParticipantCategoriesController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @ege_participant_categorys = EgeParticipantCategory.accessible_by(current_ability).paginate(page: params[:page]).order('code ASC')
    if current_user.admin?
      if !params[:search].nil?
        @ege_participant_categorys = EgeParticipantCategory.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    else
      if !params[:search].nil?
        @ege_participant_categorys = EgeParticipantCategory.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    end
  end

  def export
    @ege_participant_categorys = EgeParticipantCategory.accessible_by(current_ability).order('code ASC')
    render xlsx: 'ege_participant_categories_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/ege_participant_categories/export.xlsx.axlsx'
    # render "export.xlsx.axlsx"
  end

  def new
    @ege_participant_category = EgeParticipantCategory.new
    if !@ege_participant_category.nil?
       render :edit
    else
       flash[:success] = t("ege_participant_categorys.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @ege_participant_category = EgeParticipantCategory.accessible_by(current_ability).find(params[:id])
    @previous = EgeParticipantCategory.accessible_by(current_ability).where("id < " + @ege_participant_category.id.to_s).order("id DESC").first
    @next = EgeParticipantCategory.accessible_by(current_ability).where("id > " + @ege_participant_category.id.to_s).order("id ASC").first
    @ege_participant_categorys = EgeParticipantCategory.accessible_by(current_ability).all.order('code ASC')
    if can? :view, @ege_participant_category
      render :action => 'show'
    end
  end

  def create
    @ege_participant_category = EgeParticipantCategory.new(ege_participant_category_params)
    if @ege_participant_category.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @ege_participant_category = EgeParticipantCategory.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @ege_participant_category }
    end
  end

  def modaledit
    @ege_participant_category = EgeParticipantCategory.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @ege_participant_category }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    EgeParticipantCategory.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @ege_participant_category = EgeParticipantCategory.accessible_by(current_ability).find(params[:id])
    @ege_participant_category.destroy
    respond_to do |format|
      format.html { redirect_to ege_participant_categories_url, notice: t("ege_participant_categorys.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @ege_participant_category = EgeParticipantCategory.accessible_by(current_ability).find(params[:id])

    if @ege_participant_category.update ege_participant_category_params
      respond_to do |format|
        format.html {
          flash[:success] = t("ege_participant_categorys.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @ege_participant_category.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("ege_participant_categories." + er.attribute.to_s) + ': ' + t("ege_participant_categories.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@ege_participant_category) }
      end
    end

  end

  def ege_participant_category_params
    params.require(:ege_participant_category).permit(:code, :name, :created_at, :updated_at, :short_name, :fisgia9code, :fisgia11code, :sort_by, :require_document)
  end

  private
  def undo_link
    view_context.link_to("undo", revert_version_path(@ege_participant_category.versions.scoped.last), :method => :post)
  end

end
