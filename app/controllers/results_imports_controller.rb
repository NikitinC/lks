class ResultsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @results_import = ResultsImport.new
  end

  def create
    @results_import = ResultsImport.new(params[:results_import])
    render :create
  end

end
