class UserTestTypesImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def new
    @user_test_types_import = UserTestTypesImport.new
  end

  def create
    @user_test_types_import = UserTestTypesImport.new(params[:user_test_types_import])
    render :create
  end

end
