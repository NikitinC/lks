class AnswersController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @test_types=TestType.all
    @subjects=Subject.all
    @process_conditions=ProcessCondition.all
    @answers = Answer.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if !params[:search].nil?
      @answers = Answer.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
    end
  end

  def export
    @answers = Answer.accessible_by(current_ability).order('id ASC')
    render xlsx: 'answers_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/answers/export.xlsx.axlsx'
  end


  def new
    @test_types=TestType.all
    @subjects=Subject.all
    @process_conditions=ProcessCondition.all
    @answer = Answer.new
  end

  def show
    @answer = Answer.find(params[:id])
  end

  def create
    @test_types=TestType.all
    @subjects=Subject.all
    @process_conditions=ProcessCondition.all
    @answer = Answer.new(answer_params)
    @answer.save
    redirect_to :action => 'index'
    # if @answer.save
    #   redirect_to :action => 'index'
    # else
    #   # This line overrides the default rendering behavior, which
    #   # would have been to render the "create" view.
    #   render :action => 'new'
    # end
  end

  def edit
    @test_types=TestType.all
    @subjects=Subject.all
    @process_conditions=ProcessCondition.all
    @answer = Answer.find(params[:id])
  end

  def delete
    Answer.find(params[:id]).destroy
    redirect_to :action => 'index'
  end

  def destroy
    @answer = Answer.find(params[:id])
    @answer.destroy
    respond_to do |format|
      format.html { redirect_to answers_url, notice: t('answers.flash.destroyed') }
      format.json { head :no_content }
    end
  end


  def update
    @test_types=TestType.all
    @subjects=Subject.all
    @process_conditions=ProcessCondition.all
    @answer = Answer.find params[:id]
    # respond_to do |format|
    if @answer.update answer_params
      flash[:success] = t("answers.flash.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def answer_params
    params.require(:answer).permit(:identifier, :subject_id, :examdate, :test_type_id, :task_number,
                                   :answer_value, :answer_value)
  end

end
