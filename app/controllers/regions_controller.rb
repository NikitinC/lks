class RegionsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @regions = Region.accessible_by(current_ability).paginate(page: params[:page]).order('code ASC')
    if current_user.admin?
      if !params[:search].nil?
        @regions = Region.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    else
      if !params[:search].nil?
        @regions = Region.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    end
  end

  def export
    @regions = Region.accessible_by(current_ability).order('code ASC')
    render xlsx: 'regions_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/regions/export.xlsx.axlsx'
    # render "export.xlsx.axlsx"
  end

  def new
    @region = Region.new
    if !@region.nil?
       render :edit
    else
       flash[:success] = t("regions.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @region = Region.accessible_by(current_ability).find(params[:id])
    @previous = Region.accessible_by(current_ability).where("id < " + @region.id.to_s).order("id DESC").first
    @next = Region.accessible_by(current_ability).where("id > " + @region.id.to_s).order("id ASC").first
    @regions = Region.accessible_by(current_ability).all.order('code ASC')
    if can? :view, @region
      render :action => 'show'
    end
  end

  def create
    @region = Region.new(region_params)
    if @region.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @region = Region.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @region }
    end
  end

  def modaledit
    @region = Region.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @region }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    Region.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @region = Region.accessible_by(current_ability).find(params[:id])
    @region.destroy
    respond_to do |format|
      format.html { redirect_to regions_url, notice: t("regions.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @region = Region.accessible_by(current_ability).find(params[:id])

    if @region.update region_params
      respond_to do |format|
        format.html {
          flash[:success] = t("regions.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @region.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("regions." + er.attribute.to_s) + ': ' + t("regions.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@region) }
      end
    end

  end

  def region_params
    params.require(:region).permit(:code, :name, :created_at, :updated_at, :short_name, :fisgia9code, :fisgia11code, :sort_by)
  end

  private
  def undo_link
    view_context.link_to("undo", revert_version_path(@region.versions.scoped.last), :method => :post)
  end

end
