class AppealsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    (@filterrific = initialize_filterrific(
      Appeal.accessible_by(current_ability),
      params[:filterrific],
      select_options: {
        sorted_by: Appeal.options_for_sorted_by,
      },
    )) or return
    @appeals = @filterrific.find.page(params[:page])
    @appeal_stations = AppealStation.all
    # group_ids = Group.where(ou_id: ou_ids).map(&:id)
    ap_ids = @appeals.map(&:id)
    @test_types = TestType.where("id in (select test_type_id from appeals)").order('code ASC')
    @subjects = Subject.where("id in (select subject_id from appeals)").distinct.order('code ASC')
    @dates = Appeal.all.order('examdate DESC').map(&:examdate).uniq
    @ates = Ate.all.order('code ASC')
    @appeal_conditions = AppealCondition.all.order('ID ASC')
    # @dates = @appeals.map(&:examdate)
    # @test_types = TestType.all
    respond_to do |format|
      format.html
      format.js
    end
  rescue ActiveRecord::RecordNotFound => e
    puts "Had to reset filterriffic params: #{e.message}"
    redirect_to(reset_filterriffic_ulr(format: :html)) and return
  end

  def export
    @appeals = Appeal.accessible_by(current_ability).order('id ASC')
    render xlsx: 'appeals_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/appeals/export.xlsx.axlsx'
  end


  def new
    @appeal = Appeal.new
  end

  def show
    @appeal = Appeal.find(params[:id])
  end

  def create
    @appeal = Appeal.new(appeal_params)
    if @appeal.save
      redirect_to :action => 'index'
    else
      # This line overrides the default rendering behavior, which
      # would have been to render the "create" view.
      render :action => 'new'
    end
  end

  def edit
    @appeal = Appeal.find(params[:id])
  end

  def delete
    Appeal.find(params[:id]).destroy
    redirect_to :action => 'index'
  end

  def destroy
    @appeal = Appeal.find(params[:id])
    @appeal.destroy
    respond_to do |format|
      format.html { redirect_to appeals_url, notice: t("appeals.notice.destroyed") }
      format.json { head :no_content }
    end
  end


  def update
    @appeal = Appeal.find params[:id]
    # respond_to do |format|
    if @appeal.update appeal_params
      flash[:success] = t("appeals.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def appeal_params
    params.require(:appeal).permit(:appeal_type_id, :appeal_condition_id, :test_type_id, :subject_id, :identifier,
                                   :examdate, :appeal_station_id, :appeal_time, :appeal_link, :presence, :user_id,
                                   :notification, :delete_notification, :statement, :delete_statement)
  end

end
