class SubjectsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def new
    @subjects_import = SubjectsImport.new
  end

  def create
    @subjects_import = SubjectsImport.new(params[:subjects_import])
    render :create
  end

end
