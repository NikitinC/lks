class ExamsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!
  before_action :fill_directories

  def index
    @exams = Exam.accessible_by(current_ability).paginate(page: params[:page]).order('code ASC')
    if current_user.admin?
      if !params[:search].nil?
        @exams = Exam.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    else
      if !params[:search].nil?
        @exams = Exam.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    end
  end

  def export
    @exams = Exam.accessible_by(current_ability).order('code ASC')
    render xlsx: 'exams_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/exams/export.xlsx.axlsx'
    # render "export.xlsx.axlsx"
  end

  def new
    @exam = Exam.new
    if !@exam.nil?
       render :edit
    else
       flash[:success] = t("exams.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @exam = Exam.accessible_by(current_ability).find(params[:id])
    @previous = Exam.accessible_by(current_ability).where("id < " + @exam.id.to_s).order("id DESC").first
    @next = Exam.accessible_by(current_ability).where("id > " + @exam.id.to_s).order("id ASC").first
    @exams = Exam.accessible_by(current_ability).all.order('code ASC')
    if can? :view, @exam
      render :action => 'show'
    end
  end

  def create
    @exam = Exam.new(exam_params)
    if @exam.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @exam = Exam.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @exam }
    end
  end

  def modaledit
    @exam = Exam.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @exam }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    Exam.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @exam = Exam.accessible_by(current_ability).find(params[:id])
    @exam.destroy
    respond_to do |format|
      format.html { redirect_to exams_url, notice: t("exams.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @exam = Exam.accessible_by(current_ability).find(params[:id])

    if @exam.update exam_params
      respond_to do |format|
        format.html {
          flash[:success] = t("exams.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @exam.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("exams." + er.attribute.to_s) + ': ' + t("exams.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@exam) }
      end
    end

  end

  def exam_params
    params.require(:exam).permit(:date, :registration_expiration_date, :attestation_form_code, :subject_code,
                                 :appeal_start_date, :appeal_finish_date, :custom_fields, :exam_type_id,
                                 :skip_validation_subject, :reserved, :hidden, :code, :name, :short_name,
                                 :fisgia9code, :fisgia11code, :sort_by, :created_at, :updated_at, :deleted_at,
                                 :exam_wave_id, :subject_id, :examdate, :education_scan, :is_reserv)
  end

  private
  def fill_directories
    @exam_waves = ExamWave.all.order('sort_by')
    @subjects = Subject.all.order('code')

  end

  def undo_link
    view_context.link_to("undo", revert_version_path(@exam.versions.scoped.last), :method => :post)
  end

end
