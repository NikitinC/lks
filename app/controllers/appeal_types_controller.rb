class AppealTypesController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @appeal_types = AppealType.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if !params[:search].nil?
      @appeal_types = AppealType.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
    end
  end

  def export
    @appeal_types = AppealType.accessible_by(current_ability).order('id ASC')
    render xlsx: 'appeal_types_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/appeal_types/export.xlsx.axlsx'
  end


  def new
    @appeal_type = AppealType.new
  end

  def show
    @appeal_type = AppealType.find(params[:id])
  end

  def create
    @appeal_type = AppealType.new(appeal_type_params)
    if @appeal_type.save
      redirect_to :action => 'index'
    else
      # This line overrides the default rendering behavior, which
      # would have been to render the "create" view.
      render :action => 'new'
    end
  end

  def edit
    @appeal_type = AppealType.find(params[:id])
  end

  def delete
    AppealType.find(params[:id]).destroy
    redirect_to :action => 'index'
  end

  def destroy
    @appeal_type = AppealType.find(params[:id])
    @appeal_type.destroy
    respond_to do |format|
      format.html { redirect_to appeal_types_url, notice: t("appeal_types.notice.destroyed") }
      format.json { head :no_content }
    end
  end


  def update
    @appeal_type = AppealType.find params[:id]
    # respond_to do |format|
    if @appeal_type.update appeal_type_params
      flash[:success] = t("appeal_types.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def appeal_type_params
    params.require(:appeal_type).permit(:code, :name)
  end

end
