class QuestionTypesImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @question_types_import = QuestionTypesImport.new
  end

  def create
    @question_types_import = QuestionTypesImport.new(params[:question_types_import])
    render :create
  end

end
