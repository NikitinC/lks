class TestTypesImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def new
    @test_types_import = TestTypesImport.new
  end

  def create
    @test_types_import = TestTypesImport.new(params[:test_types_import])
    render :create
  end

end
