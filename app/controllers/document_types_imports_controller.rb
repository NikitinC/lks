class DocumentTypesImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @document_types_import = DocumentTypesImport.new
  end

  def create
    @document_types_import = DocumentTypesImport.new(params[:document_types_import])
    render :create
  end

end
