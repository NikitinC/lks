class ExamWavesController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @exam_waves = ExamWave.accessible_by(current_ability).paginate(page: params[:page]).order('code ASC')
    if current_user.admin?
      if !params[:search].nil?
        @exam_waves = ExamWave.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    else
      if !params[:search].nil?
        @exam_waves = ExamWave.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    end
  end

  def export
    @exam_waves = ExamWave.accessible_by(current_ability).order('code ASC')
    render xlsx: 'exam_waves_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/exam_waves/export.xlsx.axlsx'
    # render "export.xlsx.axlsx"
  end

  def new
    @exam_wave = ExamWave.new
    if !@exam_wave.nil?
       render :edit
    else
       flash[:success] = t("exam_waves.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @exam_wave = ExamWave.accessible_by(current_ability).find(params[:id])
    @previous = ExamWave.accessible_by(current_ability).where("id < " + @exam_wave.id.to_s).order("id DESC").first
    @next = ExamWave.accessible_by(current_ability).where("id > " + @exam_wave.id.to_s).order("id ASC").first
    @exam_waves = ExamWave.accessible_by(current_ability).all.order('code ASC')
    if can? :view, @exam_wave
      render :action => 'show'
    end
  end

  def create
    @exam_wave = ExamWave.new(exam_wave_params)
    if @exam_wave.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @exam_wave = ExamWave.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @exam_wave }
    end
  end

  def modaledit
    @exam_wave = ExamWave.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @exam_wave }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    ExamWave.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @exam_wave = ExamWave.accessible_by(current_ability).find(params[:id])
    @exam_wave.destroy
    respond_to do |format|
      format.html { redirect_to exam_waves_url, notice: t("exam_waves.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @exam_wave = ExamWave.accessible_by(current_ability).find(params[:id])

    if @exam_wave.update exam_wave_params
      respond_to do |format|
        format.html {
          flash[:success] = t("exam_waves.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @exam_wave.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("exam_waves." + er.attribute.to_s) + ': ' + t("exam_waves.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@exam_wave) }
      end
    end

  end

  def exam_wave_params
    params.require(:exam_wave).permit(:code, :name, :created_at, :updated_at, :short_name, :fisgia9code, :fisgia11code, :sort_by)
  end

  private
  def undo_link
    view_context.link_to("undo", revert_version_path(@exam_wave.versions.scoped.last), :method => :post)
  end

end
