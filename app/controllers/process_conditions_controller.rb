class ProcessConditionsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @process_conditions = ProcessCondition.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if !params[:search].nil?
      @process_conditions = ProcessCondition.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
    end
  end

  def export
    @process_conditions = ProcessCondition.accessible_by(current_ability).order('id ASC')
    render xlsx: 'process_conditions_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/process_conditions/export.xlsx.axlsx'
  end


  def new
    @process_condition = ProcessCondition.new
  end

  def show
    @process_condition = ProcessCondition.find(params[:id])
  end

  def create
    @process_condition = ProcessCondition.new(process_condition_params)
    if @process_condition.save
      redirect_to :action => 'index'
    else
      # This line overrides the default rendering behavior, which
      # would have been to render the "create" view.
      render :action => 'new'
    end
  end

  def edit
    @process_condition = ProcessCondition.find(params[:id])
  end

  def delete
    ProcessCondition.find(params[:id]).destroy
    redirect_to :action => 'index'
  end

  def destroy
    @process_condition = ProcessCondition.find(params[:id])
    @process_condition.destroy
    respond_to do |format|
      format.html { redirect_to process_conditions_url, notice: t("process_conditions.notice.destroyed") }
      format.json { head :no_content }
    end
  end


  def update
    @process_condition = ProcessCondition.find params[:id]
    # respond_to do |format|
    if @process_condition.update process_condition_params
      flash[:success] = t("process_conditions.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def process_condition_params
    params.require(:process_condition).permit(:code, :name)
  end

end
