class TeacherStatementsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def new
    @teacher_statements_import = TeacherStatementsImport.new
  end

  def create
    @teacher_statements_import = TeacherStatementsImport.new(params[:teacher_statements_import])
    render :create
  end

end
