class AnswersImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def new
    @answers_import = AnswersImport.new
  end

  def create
    @answers_import = AnswersImport.new(params[:answers_import])
    render :create
  end

end
