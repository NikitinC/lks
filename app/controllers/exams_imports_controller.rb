class ExamsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @exams_import = ExamsImport.new
  end

  def create
    @exams_import = ExamsImport.new(params[:exams_import])
    render :create
  end

end
