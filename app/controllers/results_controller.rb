class ResultsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @test_types=TestType.all
    @subjects=Subject.all
    @process_conditions=ProcessCondition.all
    @results = Result.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if !params[:search].nil?
      @results = Result.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
    end
  end

  def export
    @results = Result.accessible_by(current_ability).order('id ASC')
    render xlsx: 'results_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/results/export.xlsx.axlsx'
  end

  def no_blanks
    @results = Result.where("coalesce(blanks_present, false) = false").order('examdate DESC')
    render xlsx: 'no_blanks_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/results/no_blanks.xlsx.axlsx'
  end

  def sverka
    results = Result.where("coalesce(blanks_present, false) = false").order('examdate DESC')
    results.each do |rr|
      student = Student.where(:guid => rr.identifier).first
      subject = Subject.where(:fisgia11code => rr.subject_code).first if student.is_ege_or_gve11
      subject = Subject.where(:fisgia9code => rr.subject_code).first if student.is_oge_or_gve9
      exam = Exam.where(:date => rr.examdate, :subject_code => subject.code).first
      pdffile="/uploads/blanks/"+rr.test_type.to_s+'/'+rr.blank_r.to_s+".pdf"
      if File.exist?("#{Rails.public_path}#{pdffile}")
        rr.blanks_present = true
        rr.save
      else
        rr.blanks_present = false
        rr.save
      end
    end
    @results = Result.where("coalesce(blanks_present, false) = false").order('examdate DESC')
  end


  def new
    @test_types=TestType.all
    @subjects=Subject.all
    @process_conditions=ProcessCondition.all
    @result = Result.new
  end

  def show
    @result = Result.find(params[:id])
  end

  def create
    @test_types=TestType.all
    @subjects=Subject.all
    @process_conditions=ProcessCondition.all
    @result = Result.new(result_params)
    @result.save
    redirect_to :action => 'index'
    # if @result.save
    #   redirect_to :action => 'index'
    # else
    #   # This line overrides the default rendering behavior, which
    #   # would have been to render the "create" view.
    #   render :action => 'new'
    # end
  end

  def edit
    @test_types=TestType.all
    @subjects=Subject.all
    @process_conditions=ProcessCondition.all
    @result = Result.find(params[:id])
  end

  def delete
    Result.find(params[:id]).destroy
    redirect_to :action => 'index'
  end

  def destroy
    @result = Result.find(params[:id])
    @result.destroy
    respond_to do |format|
      format.html { redirect_to results_url, notice: t('results.flash.destroyed') }
      format.json { head :no_content }
    end
  end


  def update
    @test_types=TestType.all
    @subjects=Subject.all
    @process_conditions=ProcessCondition.all
    @result = Result.find params[:id]
    # respond_to do |format|
    if @result.update result_params
      flash[:success] = t("results.flash.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def result_params
    params.require(:result).permit(:identifier, :subject_id, :examdate, :test_type_id, :process_condition_id,
                                   :station, :variant, :primarymark, :mark100, :mark5, :testresulta,
                                   :testresultb, :testresultc, :testresultd, :blank_r, :blank_1, :blank_2)
  end

end
