class TaskSpecificationsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def new
    @task_specifications_import = TaskSpecificationsImport.new
  end

  def create
    @task_specifications_import = TaskSpecificationsImport.new(params[:task_specifications_import])
    render :create
  end

end
