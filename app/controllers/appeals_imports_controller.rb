class AppealsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def new
    @appeals_import = AppealsImport.new
  end

  def create
    @appeals_import = AppealsImport.new(params[:appeals_import])
    render :create
  end

end
