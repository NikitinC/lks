class MouosImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def new
    @mouos_import = MouosImport.new
  end

  def create
    @mouos_import = MouosImport.new(params[:mouos_import])
    render :create
  end

end
