class RegistrationPlacesImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @registration_places_import = RegistrationPlacesImport.new
  end

  def create
    @registration_places_import = RegistrationPlacesImport.new(params[:registration_places_import])
    render :create
  end

end
