class TeacherStatementsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @test_types=TestType.all
    @subjects=Subject.all
    @teacher_statements = TeacherStatement.accessible_by(current_ability).paginate(page: params[:page]).order('id DESC')
    if !params[:search].nil?
      @teacher_statements = TeacherStatement.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
    end
  end

  def export
    @teacher_statements = TeacherStatement.accessible_by(current_ability).order('id ASC')
    render xlsx: 'teacher_statements_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/teacher_statements/export.xlsx.axlsx'
  end


  def new
    @test_types=TestType.all
    @subjects=Subject.all
    @teacher_statement = TeacherStatement.new
  end

  def show
    @teacher_statement = TeacherStatement.find(params[:id])
  end

  def create
    @test_types=TestType.all
    @subjects=Subject.all
    @teacher_statement = TeacherStatement.new(teacher_statement_params)
    if @teacher_statement.save
      redirect_to :action => 'index'
    else
      # This line overrides the default rendering behavior, which
      # would have been to render the "create" view.
      render :action => 'new'
    end
  end

  def edit
    @test_types=TestType.all
    @subjects=Subject.all
    @teacher_statement = TeacherStatement.find(params[:id])
  end

  def delete
    TeacherStatement.find(params[:id]).destroy
    redirect_to :action => 'index'
  end

  def destroy
    @teacher_statement = TeacherStatement.find(params[:id])
    @teacher_statement.destroy
    respond_to do |format|
      format.html { redirect_to teacher_statements_url, notice: t("teacher_statements.notice.destroyed") }
      format.json { head :no_content }
    end
  end


  def update
    @teacher_statement = TeacherStatement.find params[:id]
    # respond_to do |format|
    if @teacher_statement.update teacher_statement_params
      flash[:success] = t("teacher_statements.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def teacher_statement_params
    params.require(:teacher_statement).permit(
      :fio, :email, :ate_id, :phone, :ou_id, :job_place,
      :subject01, :subject02, :subject03, :subject04, :subject05, :subject06, :subject07, :subject08, :subjects_other,
      :napr01, :napr02, :napr03, :napr04, :napr05, :napr06, :napr07, :napr08, :napr_other,
      :cifra, :cifra_napr01, :cifra_napr02, :cifra_napr03, :cifra_other,
      :individual, :form01, :form02, :form03, :form04, :form05, :form_other,
      :user_id
    )
  end

end