class HomeController < ApplicationController
  def index
  end

  def show_result
  end

  def show_results_list
  end

  def terms
  end

  def privacy
  end

  def help
  end
end
