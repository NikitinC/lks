class AnoncesController < ApplicationController
  Anonce.table_name = 'announcements'

  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @test_types=TestType.all
    @subjects=Subject.all
    @process_conditions=ProcessCondition.all
    @anonces = Anonce.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if !params[:search].nil?
      @anonces = Anonce.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
    end
  end

  def export
    @anonces = Anonce.accessible_by(current_ability).order('id ASC')
    render xlsx: 'anonces_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/anonces/export.xlsx.axlsx'
  end


  def new
    @test_types=TestType.all
    @subjects=Subject.all
    @process_conditions=ProcessCondition.all
    @anonce = Anonce.new
  end

  def show
    @anonce = Anonce.find(params[:id])
  end

  def create
    @test_types=TestType.all
    @subjects=Subject.all
    @process_conditions=ProcessCondition.all
    @anonce = Anonce.new(anonce_params)
    @anonce.save
    redirect_to :action => 'index'
    # if @anonce.save
    #   redirect_to :action => 'index'
    # else
    #   # This line overrides the default rendering behavior, which
    #   # would have been to render the "create" view.
    #   render :action => 'new'
    # end
  end

  def edit
    @test_types=TestType.all
    @subjects=Subject.all
    @process_conditions=ProcessCondition.all
    @anonce = Anonce.find(params[:id])
  end

  def delete
    Anonce.find(params[:id]).destroy
    redirect_to :action => 'index'
  end

  def destroy
    @anonce = Anonce.find(params[:id])
    @anonce.destroy
    respond_to do |format|
      format.html { redirect_to anonces_url, notice: t('anonces.flash.destroyed') }
      format.json { head :no_content }
    end
  end


  def update
    @test_types=TestType.all
    @subjects=Subject.all
    @process_conditions=ProcessCondition.all
    @anonce = Anonce.find params[:id]
    # respond_to do |format|
    if @anonce.update anonce_params
      flash[:success] = t("anonces.flash.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def anonce_params
    params.require(:anonce).permit(:published_at, :announcement_type, :name, :description)
  end

end
