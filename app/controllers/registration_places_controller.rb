class RegistrationPlacesController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @registration_places = RegistrationPlace.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if current_user.admin?
      if !params[:search].nil?
        @registration_places = RegistrationPlace.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    else
      if !params[:search].nil?
        @registration_places = RegistrationPlace.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    end
  end

  def export
    @registration_places = RegistrationPlace.accessible_by(current_ability).order('id ASC')
    render xlsx: 'registration_places_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/registration_places/export.xlsx.axlsx'
    # render "export.xlsx.axlsx"
  end

  def new
    @registration_place = RegistrationPlace.new
    @ates = Ate.accessible_by(current_ability).order('code ASC')
    if !@registration_place.nil?
      render :edit
    else
      flash[:success] = t("registration_places.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def show
    @registration_place = RegistrationPlace.accessible_by(current_ability).find(params[:id])
    @previous = RegistrationPlace.accessible_by(current_ability).where("id < " + @registration_place.id.to_s).order("id DESC").first
    @next = RegistrationPlace.accessible_by(current_ability).where("id > " + @registration_place.id.to_s).order("id ASC").first
    @registration_places = RegistrationPlace.accessible_by(current_ability).all.order('id ASC')
    @ates = Ate.accessible_by(current_ability).order('code ASC')
    if can? :view, @registration_place
      render :action => 'show'
    end
  end

  def create
    @registration_place = RegistrationPlace.new(registration_place_params)
    @ates = Ate.accessible_by(current_ability).order('code ASC')
    if !params[:address].nil?
      @registration_place.address = params[:address]
    end
    if @registration_place.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @registration_place = RegistrationPlace.accessible_by(current_ability).find(params[:id])
    @ates = Ate.accessible_by(current_ability).order('code ASC')
    respond_to do |format|
      format.html
      format.json { respond_modal_with @registration_place }
    end
  end

  def modaledit
    @registration_place = RegistrationPlace.accessible_by(current_ability).find(params[:id])
    @ates = Ate.accessible_by(current_ability).order('code ASC')
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @registration_place }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    RegistrationPlace.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @registration_place = RegistrationPlace.accessible_by(current_ability).find(params[:id])
    @registration_place.destroy
    respond_to do |format|
      format.html { redirect_to registration_places_url, notice: t("registration_places.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @registration_place = RegistrationPlace.accessible_by(current_ability).find(params[:id])
    @ates = Ate.accessible_by(current_ability).order('code ASC')
    if !params[:address].nil?
      @registration_place.address = params[:address]
    end

    if @registration_place.update registration_place_params
      respond_to do |format|
        format.html {
          flash[:success] = t("registration_places.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @registration_place.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("registration_places." + er.attribute.to_s) + ': ' + t("registration_places.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@registration_place) }
      end
    end

  end

  def registration_place_params
    params.require(:registration_place).permit(:name, :address, :site, :phones, :additional_info, :additional_text, :deleted_at, :ate_id)
  end

  private
  def undo_link
    view_context.link_to("undo", revert_version_path(@registration_place.versions.scoped.last), :method => :post)
  end

end
