class SubjectsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @subjects = Subject.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if !params[:search].nil?
      @subjects = Subject.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
    end
  end

  def export
    @subjects = Subject.accessible_by(current_ability).order('id ASC')
    render xlsx: 'subjects_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/subjects/export.xlsx.axlsx'
  end


  def new
    @subject = Subject.new
  end

  def show
    @subject = Subject.find(params[:id])
  end

  def create
    @subject = Subject.new(subject_params)
    if @subject.save
      redirect_to :action => 'index'
    else
      # This line overrides the default rendering behavior, which
      # would have been to render the "create" view.
      render :action => 'new'
    end
  end

  def edit
    @subject = Subject.find(params[:id])
  end

  def delete
    Subject.find(params[:id]).destroy
    redirect_to :action => 'index'
  end

  def destroy
    @subject = Subject.find(params[:id])
    @subject.destroy
    respond_to do |format|
      format.html { redirect_to subjects_url, notice: t("subjects.notice.destroyed") }
      format.json { head :no_content }
    end
  end


  def update
    @subject = Subject.find params[:id]
    # respond_to do |format|
    if @subject.update subject_params
      flash[:success] = t("subjects.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def subject_params
    params.require(:subject).permit(:code, :subject_name)
  end

end
