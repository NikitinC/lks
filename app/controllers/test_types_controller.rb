class TestTypesController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @test_types = TestType.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if !params[:search].nil?
      @test_types = TestType.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
    end
  end

  def export
    @test_types = TestType.accessible_by(current_ability).order('id ASC')
    render xlsx: 'test_types_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/test_types/export.xlsx.axlsx'
  end

  def new
    @test_type = TestType.new
  end

  def show
    @test_type = TestType.find(params[:id])
  end

  def create
    @test_type = TestType.new(test_type_params)
    if @test_type.save
      redirect_to :action => 'index'
    else
      # This line overrides the default rendering behavior, which
      # would have been to render the "create" view.
      render :action => 'new'
    end
  end

  def edit
    @test_type = TestType.find(params[:id])
  end

  def delete
    TestType.find(params[:id]).destroy
    redirect_to :action => 'index'
  end

  def destroy
    @test_type = TestType.find(params[:id])
    @test_type.destroy
    respond_to do |format|
      format.html { redirect_to test_types_url, notice: t('test_types.notice.destroyed') }
      format.json { head :no_content }
    end
  end


  def update
    @test_type = TestType.find params[:id]
    # respond_to do |format|
    if @test_type.update test_type_params
      flash[:success] = t("test_types.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def test_type_params
    params.require(:test_type).permit(:code, :name)
  end

end
