class EgeStatementsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!
  before_action :fill_directories

  def index
    @ege_statements = EgeStatement.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if current_user.admin?
      if !params[:search].nil?
        @ege_statements = EgeStatement.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    else
      if !params[:search].nil?
        @ege_statements = EgeStatement.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    end
  end

  def get_registration_places
    @ate = Ate.find params[:ate_id]
    @registration_places = RegistrationPlace.where(:ate_id => params[:ate_id]).all
  end

  def export
    @ege_statements = EgeStatement.accessible_by(current_ability).order('id ASC')
    render xlsx: 'ege_statements_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/ege_statements/export.xlsx.axlsx'
    # render "export.xlsx.axlsx"
  end

  def new
    @ege_statement = EgeStatement.new

    # @ates = Ate.all
    # @registration_places = RegistrationPlace.all.order("address ASC")
    # @document_types = DocumentType.all.order("sort_by")
    # @citizenships = Citizenship.all.order("sort_by")
    # @regions = Region.all.order("sort_by")
    # @ege_participant_categories = EgeParticipantCategory.all.order("sort_by")
    # @companies = Ate.all
    # @contacts = RegistrationPlace.all
    @ege_statement.user_id = current_user.id
    @ege_statement.rus = false
    @ege_statement.math = false
    @ege_statement.phys = false
    @ege_statement.chem = false
    @ege_statement.bio = false
    @ege_statement.his = false
    @ege_statement.geo = false
    @ege_statement.lit = false
    @ege_statement.soc = false
    @ege_statement.eng = false
    @ege_statement.engu = false
    @ege_statement.deu = false
    @ege_statement.deuu = false
    @ege_statement.fra = false
    @ege_statement.frau = false
    @ege_statement.isp = false
    @ege_statement.ispu = false
    @ege_statement.kit = false
    @ege_statement.kitu = false
    @ege_statement.inf = false
    @ege_statement.pdn = false
    @ege_statement.low = false
    @ege_statement.noadd = false
    @ege_statement.processed = false

    if @ege_statement.save
      redirect_to "/ege_statements/#{@ege_statement.id}/edit"
    else
       flash[:success] = t("ege_statements.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @ege_statement = EgeStatement.accessible_by(current_ability).find(params[:id])
    @previous = EgeStatement.accessible_by(current_ability).where("id < " + @ege_statement.id.to_s).order("id DESC").first
    @next = EgeStatement.accessible_by(current_ability).where("id > " + @ege_statement.id.to_s).order("id ASC").first
    @ege_statements = EgeStatement.accessible_by(current_ability).all.order('id ASC')
    # if can? :view, @ege_statement
    #   render :action => 'show'
    # end
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Заявление No. #{@ege_statement.id}",
               page_size: 'A4',
               template: "ege_statements/generatepdf.html.haml",
               layout: "pdf.html",
               orientation: "Portrait",
               lowquality: true,
               zoom: 1,
               dpi: 75
      end
    end
  end

  def create
    @ege_statement = EgeStatement.new(ege_statement_params)
    if @ege_statement.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @ege_statement = EgeStatement.accessible_by(current_ability).find(params[:id])

    respond_to do |format|
      format.html
      format.json { respond_modal_with @ege_statement }
    end
  end

  def modaledit
    @ege_statement = EgeStatement.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @ege_statement }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    EgeStatement.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @ege_statement = EgeStatement.accessible_by(current_ability).find(params[:id])
    @ege_statement.destroy
    respond_to do |format|
      format.html { redirect_to ege_statements_url, notice: t("ege_statements.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @ege_statement = EgeStatement.accessible_by(current_ability).find(params[:id])

    if @ege_statement.update ege_statement_params
      respond_to do |format|
        format.html {
          flash[:success] = t("ege_statements.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @ege_statement.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("ege_statements." + er.attribute.to_s) + ': ' + t("ege_statements.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@ege_statement) }
      end
    end

  end

  def ege_statement_params
    params.require(:ege_statement).permit(:user_id, :registration_place_id, :surname, :name, :second_name, :document_type_id,
                                          :document_series, :document_number, :birthday, :gender, :citizenship_id, :snils, :phone,
                                          :ege_participant_category_id, :gia_active_results_rus, :gia_active_results_math,
                                          :region_id, :education_scan, :deleted_at,
                                          :school_outcoming, :school_outcoming_year,  :citizenship_country,
                                          :document_vydan_kem, :document_vydan_kogda,
                                          :education_series, :education_number, :education_vydan_kem, :education_vydan_kogda,
                                          :special_seating, :is_ovz, :ate_id,
                                          :rus, :rus_date, :math, :math_date, :phys, :phys_date, :chem, :chem_date, :inf, :inf_date,
                                          :bio, :bio_date, :his, :his_date, :geo, :geo_date,
                                          :eng, :eng_date, :engu, :engu_date,
                                          :deu, :deu_date, :deuu, :deuu_date,
                                          :fra, :fra_date, :frau, :frau_date,
                                          :isp, :isp_date, :ispu, :ispu_date,
                                          :kit, :kit_date, :kitu, :kitu_date,
                                          :lit, :lit_date, :soc, :soc_date,
                                          :pdn, :low, :noadd, :processed
    )
  end

  private

  def fill_directories
    @ates = Ate.all
    @registration_places = RegistrationPlace.all.order("address ASC")
    @document_types = DocumentType.all.order("sort_by")
    @citizenships = Citizenship.all.order("sort_by")
    @regions = Region.all.order("sort_by")
    @ege_participant_categories = EgeParticipantCategory.all.order("sort_by")
    @exams = Exam.all.order("examdate")
  end


  def undo_link
    view_context.link_to("undo", revert_version_path(@ege_statement.versions.scoped.last), :method => :post)
  end

end