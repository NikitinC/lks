class DocumentTypesController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @document_types = DocumentType.accessible_by(current_ability).paginate(page: params[:page]).order('code ASC')
    if current_user.admin?
      if !params[:search].nil?
        @document_types = DocumentType.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    else
      if !params[:search].nil?
        @document_types = DocumentType.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    end
  end

  def export
    @document_types = DocumentType.accessible_by(current_ability).order('code ASC')
    render xlsx: 'document_types_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/document_types/export.xlsx.axlsx'
    # render "export.xlsx.axlsx"
  end

  def new
    @document_type = DocumentType.new
    if !@document_type.nil?
       render :edit
    else
       flash[:success] = t("document_types.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @document_type = DocumentType.accessible_by(current_ability).find(params[:id])
    @previous = DocumentType.accessible_by(current_ability).where("id < " + @document_type.id.to_s).order("id DESC").first
    @next = DocumentType.accessible_by(current_ability).where("id > " + @document_type.id.to_s).order("id ASC").first
    @document_types = DocumentType.accessible_by(current_ability).all.order('code ASC')
    if can? :view, @document_type
      render :action => 'show'
    end
  end

  def create
    @document_type = DocumentType.new(document_type_params)
    if @document_type.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @document_type = DocumentType.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @document_type }
    end
  end

  def modaledit
    @document_type = DocumentType.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @document_type }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    DocumentType.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @document_type = DocumentType.accessible_by(current_ability).find(params[:id])
    @document_type.destroy
    respond_to do |format|
      format.html { redirect_to document_types_url, notice: t("document_types.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @document_type = DocumentType.accessible_by(current_ability).find(params[:id])

    if @document_type.update document_type_params
      respond_to do |format|
        format.html {
          flash[:success] = t("document_types.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @document_type.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("document_types." + er.attribute.to_s) + ': ' + t("document_types.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@document_type) }
      end
    end

  end

  def document_type_params
    params.require(:document_type).permit(:code, :name, :created_at, :updated_at, :short_name, :fisgia9code, :fisgia11code, :sort_by, :series_mask, :number_mask, :series_example, :number_example, :fct_ege_code, :fct_oge_code, :fias_code)
  end

  private
  def undo_link
    view_context.link_to("undo", revert_version_path(@document_type.versions.scoped.last), :method => :post)
  end

end
