class ApplicationController < ActionController::Base
  include Pundit

  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :masquerade_user!


  # rescue_from StandardError,
  #            :with => :render_error
  # rescue_from ActionController::RoutingError,
  #            :with => :render_not_found
  # rescue_from AbstractController::ActionNotFound,
  #            :with => :render_not_found
  # rescue_from CanCan::AccessDenied do |exception|
  #   redirect_to main_app.root_url, :alert => exception.message
  # end

  def render_not_found(exception)
    render :template => "layouts/errors/404",
           :layout => 'errors/404.html.haml',
           :status => 404
  end

  def render_error(exception)
    @exception = exception
    render :template => "layouts/errors/500",
           :layout => 'errors/500.html.haml',
           :status => 500
  end


  def respond_modal_with(*args, &blk)
    options = args.extract_options!
    options[:responder] = ModalResponder
    respond_with *args, options, &blk
  end

 
  protected


    def configure_permitted_parameters
      attributes = [:first_name, :email, :password, :password_confirmation, :superadmin_role => false, :mouo_role => false, :ous_role => false, :expert_role => false, :dpo_role => false, :blank_role => false]
      attributes2 = [:first_name, :email, :password, :password_confirmation]
      devise_parameter_sanitizer.permit(:sign_up, keys: attributes)
      devise_parameter_sanitizer.permit(:account_update, keys: attributes2)
    end
end
