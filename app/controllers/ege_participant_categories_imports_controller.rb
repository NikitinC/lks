class EgeParticipantCategoriesImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @ege_participant_categories_import = EgeParticipantCategoriesImport.new
  end

  def create
    @ege_participant_categories_import = EgeParticipantCategoriesImport.new(params[:ege_participant_categories_import])
    render :create
  end

end
