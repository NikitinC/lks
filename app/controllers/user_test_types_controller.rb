class UserTestTypesController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @test_types=TestType.all
    @user_test_types = UserTestType.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if !params[:search].nil?
      @user_test_types = UserTestType.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
    end
  end

  def export
    @user_test_types = UserTestType.accessible_by(current_ability).order('id ASC')
    render xlsx: 'user_test_types_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/user_test_types/export.xlsx.axlsx'
  end


  def new
    @test_types=TestType.all
    @user_test_type = UserTestType.new
  end

  def show
    @user_test_type = UserTestType.find(params[:id])
  end

  def create
    @test_types=TestType.all
    @user_test_type = UserTestType.new(user_test_type_params)
    if @user_test_type.save
      redirect_to :action => 'index'
    else
      # This line overrides the default rendering behavior, which
      # would have been to render the "create" view.
      render :action => 'new'
    end
  end

  def edit
    @test_types=TestType.all
    @user_test_type = UserTestType.find(params[:id])
  end

  def delete
    UserTestType.find(params[:id]).destroy
    redirect_to :action => 'index'
  end

  def destroy
    @user_test_type = UserTestType.find(params[:id])
    @user_test_type.destroy
    respond_to do |format|
      format.html { redirect_to user_test_types_url, notice: t("user_test_types.notice.destroyed") }
      format.json { head :no_content }
    end
  end


  def update
    @user_test_type = UserTestType.find params[:id]
    @test_types=TestType.all
    # respond_to do |format|
    if @user_test_type.update user_test_type_params
      flash[:success] = t("user_test_types.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def user_test_type_params
    @test_types=TestType.all
    params.require(:user_test_type).permit(:user_id, :test_type_id, :identifier)
  end

end
