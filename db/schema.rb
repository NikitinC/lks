# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_06_05_132910) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "dblink"
  enable_extension "plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "announcements", force: :cascade do |t|
    t.datetime "published_at"
    t.string "announcement_type"
    t.string "name"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "answers", force: :cascade do |t|
    t.bigint "test_type_id"
    t.date "examdate"
    t.bigint "subject_id"
    t.string "identifier", limit: 40
    t.integer "task_number"
    t.string "answer_value", limit: 100
    t.string "replace_value", limit: 100
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["id"], name: "index_answers_on_id", unique: true
    t.index ["identifier"], name: "identifier"
    t.index ["subject_id"], name: "index_answers_on_subject_id"
    t.index ["test_type_id", "examdate", "subject_id"], name: "result_id"
    t.index ["test_type_id"], name: "index_answers_on_test_type_id"
  end

  create_table "appeal_conditions", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "appeal_stations", force: :cascade do |t|
    t.bigint "mouo_id"
    t.string "name"
    t.string "address"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["mouo_id"], name: "index_appeal_stations_on_mouo_id"
  end

  create_table "appeal_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["id"], name: "index_appeal_types_on_id", unique: true
  end

  create_table "appeals", force: :cascade do |t|
    t.bigint "appeal_type_id"
    t.bigint "appeal_condition_id"
    t.bigint "test_type_id"
    t.bigint "subject_id"
    t.string "identifier", limit: 40
    t.date "examdate"
    t.bigint "appeal_station_id"
    t.datetime "appeal_time"
    t.string "appeal_link"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "presence"
    t.bigint "user_id"
    t.index ["appeal_condition_id"], name: "index_appeals_on_appeal_condition_id"
    t.index ["appeal_station_id"], name: "index_appeals_on_appeal_station_id"
    t.index ["appeal_type_id"], name: "index_appeals_on_appeal_type_id"
    t.index ["id"], name: "index_appeals_on_id", unique: true
    t.index ["subject_id"], name: "index_appeals_on_subject_id"
    t.index ["test_type_id"], name: "index_appeals_on_test_type_id"
    t.index ["user_id"], name: "index_appeals_on_user_id"
  end

  create_table "ates", force: :cascade do |t|
    t.integer "code"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "citizenships", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "short_name"
    t.integer "fisgia9code"
    t.integer "fisgia11code"
    t.integer "sort_by"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_citizenships_on_code"
  end

  create_table "document_types", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "short_name"
    t.integer "fisgia9code"
    t.integer "fisgia11code"
    t.integer "sort_by"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "series_mask"
    t.string "number_mask"
    t.string "series_example"
    t.string "number_example"
    t.integer "fct_ege_code"
    t.integer "fct_oge_code"
    t.integer "fias_code"
    t.index ["code"], name: "index_document_types_on_code"
  end

  create_table "ege_participant_categories", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "short_name"
    t.integer "fisgia9code"
    t.integer "fisgia11code"
    t.integer "sort_by"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_ege_participant_categories_on_code"
  end

  create_table "ege_statements", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "registration_place_id"
    t.string "surname"
    t.string "name"
    t.string "second_name"
    t.bigint "document_type_id"
    t.string "document_series"
    t.string "document_number"
    t.date "birthday"
    t.boolean "gender"
    t.bigint "citizenship_id"
    t.string "snils"
    t.string "phone"
    t.bigint "ege_participant_category_id"
    t.boolean "gia_active_results_rus"
    t.boolean "gia_active_results_math"
    t.bigint "region_id"
    t.string "education_scan"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "school_outcoming"
    t.string "school_outcoming_year"
    t.string "citizenship_country"
    t.string "document_vydan_kem"
    t.datetime "document_vydan_kogda"
    t.string "education_series"
    t.string "education_number"
    t.string "education_vydan_kem"
    t.string "education_vydan_kogda"
    t.boolean "special_seating"
    t.boolean "is_ovz"
    t.bigint "ate_id"
    t.boolean "rus"
    t.datetime "rus_date"
    t.boolean "math"
    t.datetime "math_date"
    t.boolean "phys"
    t.datetime "phys_date"
    t.boolean "chem"
    t.datetime "chem_date"
    t.boolean "inf"
    t.datetime "inf_date"
    t.boolean "bio"
    t.datetime "bio_date"
    t.boolean "his"
    t.datetime "his_date"
    t.boolean "geo"
    t.datetime "geo_date"
    t.boolean "eng"
    t.datetime "eng_date"
    t.boolean "engu"
    t.datetime "engu_date"
    t.boolean "deu"
    t.datetime "deu_date"
    t.boolean "deuu"
    t.datetime "deuu_date"
    t.boolean "fra"
    t.datetime "fra_date"
    t.boolean "frau"
    t.datetime "frau_date"
    t.boolean "isp"
    t.datetime "isp_date"
    t.boolean "ispu"
    t.datetime "ispu_date"
    t.boolean "kit"
    t.datetime "kit_date"
    t.boolean "kitu"
    t.datetime "kitu_date"
    t.boolean "lit"
    t.datetime "lit_date"
    t.boolean "soc"
    t.datetime "soc_date"
    t.string "address"
    t.boolean "pdn"
    t.boolean "low"
    t.boolean "noadd"
    t.boolean "processed"
    t.index ["ate_id"], name: "index_ege_statements_on_ate_id"
    t.index ["citizenship_id"], name: "index_ege_statements_on_citizenship_id"
    t.index ["document_type_id"], name: "index_ege_statements_on_document_type_id"
    t.index ["ege_participant_category_id"], name: "index_ege_statements_on_ege_participant_category_id"
    t.index ["region_id"], name: "index_ege_statements_on_region_id"
    t.index ["registration_place_id"], name: "index_ege_statements_on_registration_place_id"
    t.index ["user_id"], name: "index_ege_statements_on_user_id"
  end

  create_table "exam_waves", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "short_name"
    t.integer "fisgia9code"
    t.integer "fisgia11code"
    t.integer "sort_by"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_exam_waves_on_code"
  end

  create_table "exams", force: :cascade do |t|
    t.bigint "exam_wave_id", null: false
    t.bigint "subject_id", null: false
    t.date "examdate"
    t.boolean "is_reserv"
    t.string "code"
    t.string "name"
    t.string "short_name"
    t.integer "fisgia9code"
    t.integer "fisgia11code"
    t.integer "sort_by"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_exams_on_code"
    t.index ["exam_wave_id"], name: "index_exams_on_exam_wave_id"
    t.index ["subject_id"], name: "index_exams_on_subject_id"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "monitoring_objectivity_ates", force: :cascade do |t|
    t.integer "mouo_id"
    t.integer "user_id"
    t.integer "status_id"
    t.string "url01"
    t.string "url02"
    t.string "url03"
    t.string "url04"
    t.string "url05"
    t.string "url06"
    t.string "url07"
    t.string "url08"
    t.string "url09"
    t.string "url10"
    t.string "url11"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "monitoring_objectivity_ous", force: :cascade do |t|
    t.integer "user_id"
    t.integer "status_id"
    t.string "url01"
    t.string "document01"
    t.string "url02"
    t.string "url03"
    t.string "url04"
    t.string "url05"
    t.string "url06"
    t.string "url07"
    t.string "url08"
    t.string "url09"
    t.string "url10"
    t.string "url11"
    t.string "url12"
    t.string "url13"
    t.string "url14"
    t.string "url15"
    t.string "url16"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "ou_id"
    t.index ["ou_id"], name: "index_monitoring_objectivity_ous_on_ou_id"
  end

  create_table "mouos", force: :cascade do |t|
    t.bigint "ate_id"
    t.integer "code"
    t.string "name"
    t.string "contact_fio"
    t.string "contact_phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ate_id"], name: "index_mouos_on_ate_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.string "recipient_type", null: false
    t.bigint "recipient_id", null: false
    t.string "type", null: false
    t.jsonb "params"
    t.datetime "read_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["read_at"], name: "index_notifications_on_read_at"
    t.index ["recipient_type", "recipient_id"], name: "index_notifications_on_recipient"
  end

  create_table "objectivity_ate_marks", force: :cascade do |t|
    t.bigint "monitoring_objectivity_ate_id"
    t.bigint "user_id"
    t.integer "status"
    t.integer "mark01"
    t.integer "mark02"
    t.integer "mark03"
    t.integer "mark04"
    t.integer "mark05"
    t.integer "mark06"
    t.integer "mark07"
    t.integer "mark08"
    t.integer "mark09"
    t.integer "mark10"
    t.integer "mark11"
    t.string "notes"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["monitoring_objectivity_ate_id"], name: "index_objectivity_ate_marks_on_monitoring_objectivity_ate_id"
    t.index ["user_id"], name: "index_objectivity_ate_marks_on_user_id"
  end

  create_table "objectivity_ou_marks", force: :cascade do |t|
    t.bigint "monitoring_objectivity_ou_id"
    t.bigint "user_id"
    t.integer "status"
    t.integer "mark01"
    t.integer "mark02"
    t.integer "mark03"
    t.integer "mark04"
    t.integer "mark05"
    t.integer "mark06"
    t.integer "mark07"
    t.integer "mark08"
    t.integer "mark09"
    t.integer "mark10"
    t.integer "mark11"
    t.integer "mark12"
    t.integer "mark13"
    t.integer "mark14"
    t.integer "mark15"
    t.integer "mark16"
    t.string "notes"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["monitoring_objectivity_ou_id"], name: "index_objectivity_ou_marks_on_monitoring_objectivity_ou_id"
    t.index ["user_id"], name: "index_objectivity_ou_marks_on_user_id"
  end

  create_table "ous", force: :cascade do |t|
    t.bigint "ate_id"
    t.bigint "mouo_id"
    t.integer "code"
    t.string "name"
    t.string "contact_fio"
    t.string "contact_phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ate_id"], name: "index_ous_on_ate_id"
    t.index ["mouo_id"], name: "index_ous_on_mouo_id"
  end

  create_table "process_conditions", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["id"], name: "index_process_conditions_on_id", unique: true
    t.index ["name"], name: "index_process_conditions_on_name"
  end

  create_table "regions", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "short_name"
    t.integer "fisgia9code"
    t.integer "fisgia11code"
    t.integer "sort_by"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_regions_on_code"
  end

  create_table "registration_places", force: :cascade do |t|
    t.bigint "ate_id", null: false
    t.string "name"
    t.string "address"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "phones"
    t.string "site"
    t.string "code"
    t.index ["ate_id"], name: "index_registration_places_on_ate_id"
  end

  create_table "results", force: :cascade do |t|
    t.bigint "subject_id"
    t.date "examdate"
    t.bigint "test_type_id"
    t.bigint "process_condition_id"
    t.string "station"
    t.integer "variant"
    t.integer "primarymark"
    t.integer "mark100"
    t.integer "mark5"
    t.string "testresulta"
    t.string "testresultb"
    t.string "testresultc"
    t.string "testresultd"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "blank_r"
    t.string "blank_1"
    t.string "blank_2"
    t.string "identifier", limit: 40
    t.index ["process_condition_id"], name: "index_results_on_process_condition_id"
    t.index ["subject_id"], name: "index_results_on_subject_id"
    t.index ["test_type_id", "subject_id", "identifier"], name: "result_test_type_subject_identfier", unique: true
    t.index ["test_type_id"], name: "index_results_on_test_type_id"
  end

  create_table "services", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "provider"
    t.string "uid"
    t.string "access_token"
    t.string "access_token_secret"
    t.string "refresh_token"
    t.datetime "expires_at"
    t.text "auth"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_services_on_user_id"
  end

  create_table "subjects", force: :cascade do |t|
    t.integer "code"
    t.string "subject_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_subjects_on_code"
    t.index ["id"], name: "index_subjects_on_id", unique: true
    t.index ["subject_name"], name: "index_subjects_on_subject_name"
  end

  create_table "tables", force: :cascade do |t|
    t.integer "columns", default: 1
    t.integer "rows", default: 1
    t.json "data", default: {}
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "task_recomendations", force: :cascade do |t|
    t.bigint "test_type_id"
    t.bigint "subject_id"
    t.integer "task_number"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["id"], name: "index_task_recomendations_on_id", unique: true
    t.index ["subject_id"], name: "index_task_recomendations_on_subject_id"
    t.index ["test_type_id", "subject_id", "task_number"], name: "test_type_subject_task_number"
    t.index ["test_type_id"], name: "index_task_recomendations_on_test_type_id"
  end

  create_table "task_specifications", force: :cascade do |t|
    t.bigint "test_type_id"
    t.bigint "subject_id"
    t.integer "task_number"
    t.integer "max_ball"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["subject_id"], name: "index_task_specifications_on_subject_id"
    t.index ["test_type_id"], name: "index_task_specifications_on_test_type_id"
  end

  create_table "teacher_statements", force: :cascade do |t|
    t.string "fio"
    t.string "email"
    t.bigint "ate_id"
    t.string "phone"
    t.bigint "ou_id"
    t.string "job_place"
    t.boolean "subject01"
    t.boolean "subject02"
    t.boolean "subject03"
    t.boolean "subject04"
    t.boolean "subject05"
    t.boolean "subject06"
    t.boolean "subject07"
    t.boolean "subject08"
    t.string "subjects_other"
    t.boolean "napr01"
    t.boolean "napr02"
    t.boolean "napr03"
    t.boolean "napr04"
    t.boolean "napr05"
    t.boolean "napr06"
    t.boolean "napr07"
    t.boolean "napr08"
    t.string "napr_other"
    t.integer "cifra"
    t.boolean "cifra_napr01"
    t.boolean "cifra_napr02"
    t.boolean "cifra_napr03"
    t.string "cifra_other"
    t.string "individual"
    t.boolean "form01"
    t.boolean "form02"
    t.boolean "form03"
    t.boolean "form04"
    t.boolean "form05"
    t.string "form_other"
    t.bigint "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ate_id"], name: "index_teacher_statements_on_ate_id"
    t.index ["ou_id"], name: "index_teacher_statements_on_ou_id"
    t.index ["user_id"], name: "index_teacher_statements_on_user_id"
  end

  create_table "test_types", force: :cascade do |t|
    t.integer "code"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["id"], name: "index_test_types_on_id", unique: true
    t.index ["name"], name: "index_test_types_on_name"
  end

  create_table "user_test_types", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "test_type_id"
    t.string "identifier", limit: 40
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["id"], name: "index_user_test_types_on_id", unique: true
    t.index ["test_type_id"], name: "index_user_test_types_on_test_type_id"
    t.index ["user_id", "test_type_id", "identifier"], name: "user_test_type_identfier", unique: true
    t.index ["user_id"], name: "index_user_test_types_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "first_name"
    t.string "last_name"
    t.datetime "announcements_last_read_at"
    t.boolean "admin", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "target_type"
    t.integer "target_id"
    t.boolean "superadmin_role", default: false
    t.boolean "mouo_role", default: false
    t.boolean "ous_role", default: true
    t.boolean "expert_role", default: true
    t.boolean "dpo_role", default: true
    t.boolean "blank_role", default: true
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "ege_statements", "ates"
  add_foreign_key "ege_statements", "citizenships"
  add_foreign_key "ege_statements", "document_types"
  add_foreign_key "ege_statements", "ege_participant_categories"
  add_foreign_key "ege_statements", "regions"
  add_foreign_key "ege_statements", "registration_places"
  add_foreign_key "ege_statements", "users"
  add_foreign_key "exams", "exam_waves"
  add_foreign_key "exams", "subjects"
  add_foreign_key "monitoring_objectivity_ous", "ous"
  add_foreign_key "mouos", "ates"
  add_foreign_key "ous", "ates"
  add_foreign_key "ous", "mouos"
  add_foreign_key "registration_places", "ates"
  add_foreign_key "services", "users"
end
