class CreateTaskRecomendations < ActiveRecord::Migration[6.1]
  def change
    create_table :task_recomendations do |t|
      t.belongs_to :test_type
      t.belongs_to :subject
      t.integer :task_number
      t.timestamps
    end
    add_index :task_recomendations, :id,                unique: true
    add_index :task_recomendations, [:test_type_id, :subject_id, :task_number], name: 'test_type_subject_task_number'
  end
end
