class CreateChangeIndexes < ActiveRecord::Migration[6.1]
  def change
    remove_index :results, name: 'result_test_type_identfier'
    add_index :results, [:test_type_id, :subject_id, :identifier], unique: true, name: 'result_test_type_subject_identfier'
  end
end
