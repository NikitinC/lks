class DeleteUserFromResults < ActiveRecord::Migration[6.1]
  def change
    remove_column :results, :user_id
    remove_column :results, :participantid
    add_column :results, :blank_r, :string
    add_column :results, :blank_1, :string
    add_column :results, :blank_2, :string
    add_column :results, :identifier, :string, :limit => 40
  end
  # add_index :results, [:test_type_id, :identifier], unique: true, name: 'result_test_type_identfier'
end
