class CreateLks < ActiveRecord::Migration[6.1]
  def change
    create_table :subjects do |t|
      # t.integer :id, null: false
      t.integer :code
      t.string :subject_name
      t.timestamps
    end
    add_index :subjects, :id,                unique: true
    add_index :subjects, :code
    add_index :subjects, :subject_name
  end

  create_table :test_types do |t|
    # t.integer :id, null: false
    t.integer :code
    t.string :name
    t.timestamps
  end
  add_index :test_types, :id,                unique: true
  add_index :test_types, :name

  create_table :results do |t|
    t.belongs_to :user
    t.string :participantid
    t.belongs_to :subject
    t.date  :examdate
    t.belongs_to :test_type
    t.belongs_to :process_condition
    t.string  :station
    t.integer :variant
    t.integer :primarymark
    t.integer :mark100
    t.integer :mark5
    t.string :testresulta
    t.string :testresultb
    t.string :testresultc
    t.string :testresultd
    t.timestamps
  end

  create_table :task_specification do |t|
    t.belongs_to :test_type
    t.belongs_to :subject
    t.integer :task_number
    t.string :competention_type
    t.string :competention_name
    t.string :zun_name
    t.integer :max_ball
    t.timestamps
  end

  create_table :process_conditions do |t|
    # t.integer :id, null: false
    t.string :name
    t.timestamps
  end
  add_index :process_conditions, :id,                unique: true
  add_index :process_conditions, :name


end
