class AddPresenceToAppeal < ActiveRecord::Migration[6.1]
  def change
    add_column :appeals, :presence, :boolean
  end
end
