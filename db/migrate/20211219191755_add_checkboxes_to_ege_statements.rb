class AddCheckboxesToEgeStatements < ActiveRecord::Migration[6.1]
  def change
    add_column :ege_statements, :pdn, :boolean
    add_column :ege_statements, :low, :boolean
    add_column :ege_statements, :noadd, :boolean
    add_column :ege_statements, :processed, :boolean
  end
end
