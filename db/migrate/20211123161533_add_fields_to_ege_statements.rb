class AddFieldsToEgeStatements < ActiveRecord::Migration[6.1]
  def change
    add_column :ege_statements, :school_outcoming, :string
    add_column :ege_statements, :school_outcoming_year, :string
    add_column :ege_statements, :citizenship_country, :string
    add_column :ege_statements, :document_vydan_kem, :string
    add_column :ege_statements, :document_vydan_kogda, :datetime
    add_column :ege_statements, :education_series, :string
    add_column :ege_statements, :education_number, :string
    add_column :ege_statements, :education_vydan_kem, :string
    add_column :ege_statements, :education_vydan_kogda, :string
    add_column :ege_statements, :special_seating, :boolean
    add_column :ege_statements, :is_ovz, :boolean
    add_reference :ege_statements, :ate, null: true, foreign_key: true
    change_column_null :ege_statements, :ege_participant_category_id, null: true
    change_column_null :ege_statements, :region_id, null: true
    change_column_null :ege_statements, :citizenship_id, null: true
    change_column_null :ege_statements, :document_type_id, null: true
    change_column_null :ege_statements, :registration_place_id, null: true
  end
end
