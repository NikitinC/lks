class DeleteTargetIdFromMonitoringObjectivityOus < ActiveRecord::Migration[6.1]
  def change
    remove_column :monitoring_objectivity_ous, :target_id
  end
end
