class AddOuIdToMonitoringObjectivityOus < ActiveRecord::Migration[6.1]
  def change
    add_reference :monitoring_objectivity_ous, :ou, references: :ou, foreign_key: true
  end
end
