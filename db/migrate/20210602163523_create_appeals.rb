class CreateAppeals < ActiveRecord::Migration[6.1]
  def change
    create_table :appeal_types do |t|
      t.string :name
      t.timestamps
    end
    create_table :appeal_conditions do |t|
      t.string :name
      t.timestamps
    end
    create_table :appeal_stations do |t|
      t.belongs_to  :mouo
      t.string   :name
      t.string   :address
      t.timestamps
    end
    create_table :appeals do |t|
      t.belongs_to :appeal_type
      t.belongs_to :appeal_condition
      t.belongs_to :test_type
      t.belongs_to :subject
      t.string  :identifier, :limit => 40
      t.date :examdate
      t.belongs_to :appeal_station
      t.datetime   :appeal_time
      t.string :appeal_link
      t.timestamps
    end
    add_index :appeal_types, :id,           unique: true
    add_index :appeals, :id,                unique: true
  end
end
