class CreateExams < ActiveRecord::Migration[6.1]
  def change
    drop_table :exams

    create_table :exams do |t|
      t.belongs_to :exam_wave, null: false, foreign_key: true
      t.belongs_to :subject, null: false, foreign_key: true
      t.date :examdate
      t.boolean :is_reserv
      t.string :code
      t.string :name
      t.string :short_name
      t.integer :fisgia9code
      t.integer :fisgia11code
      t.integer :sort_by
      t.datetime :deleted_at
      t.timestamps
    end
    add_index :exams, :code
  end
end
