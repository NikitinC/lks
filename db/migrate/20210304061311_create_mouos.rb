class CreateMouos < ActiveRecord::Migration[6.1]
  def change
    create_table :mouos do |t|
      t.integer :ate_id
      t.integer :code
      t.string  :name
      t.string  :contact_fio
      t.string  :contact_phone
      t.timestamps
    end
  end
end
