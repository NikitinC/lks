class ChangeTaskSpecificationToTaskSpecifications < ActiveRecord::Migration[6.1]
  def change
    rename_table :task_specification, :task_specifications
  end
end
