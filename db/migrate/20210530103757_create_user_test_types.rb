class CreateUserTestTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :user_test_types do |t|
      t.belongs_to :user
      t.belongs_to :test_type
      t.string :identifier, :limit => 40
      t.timestamps
    end
    add_index :user_test_types, :id,                unique: true
    add_index :user_test_types, [:user_id, :test_type_id, :identifier], unique: true, name: 'user_test_type_identfier'
  end
end
