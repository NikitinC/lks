class CreateOus < ActiveRecord::Migration[6.1]
  def change
    create_table :ous do |t|
      t.integer :mouo_id
      t.integer :code
      t.string  :short_name
      t.string  :contact_fio
      t.string  :contact_phone
      t.timestamps
    end
  end
end
