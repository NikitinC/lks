class CreateCbt < ActiveRecord::Migration[6.1]
  # def change
  #
  #   #Ответ: Отметки за свободные ответы
  #   create_table :answer_types do |t|
  #     t.string      :name
  #     t.datetime    :deleted_at
  #     t.timestamps
  #   end
  #   add_index :answer_types, :id,                unique: true
  #
  #   #Тип вопроса: Вопрос в виде PDF-файла
  #   create_table :question_types do |t|
  #     t.string      :name
  #     t.datetime    :deleted_at
  #     t.timestamps
  #   end
  #   add_index :question_types, :id,                unique: true
  # end
  #
  # #Набор вопросов (например, Вопрос №1)
  # create_table :questions_sets do |t|
  #   t.string      :name
  #   t.text        :description                  # описание
  #   t.boolean     :shuffle                      # случайный набор
  #   t.belongs_to  :subject
  #   t.datetime    :deleted_at
  #   t.timestamps
  # end
  # add_index :questions_sets, :id,                unique: true
  #
  # #Вопрос: Сам вопрос
  # create_table :questions do |t|
  #   t.string      :name
  #   t.belongs_to  :subject
  #   t.belongs_to  :answer_type
  #   t.belongs_to  :question_type
  #   t.text        :settings
  #   t.float       :primary_mark
  #   t.text        :correct_answer
  #   t.boolean     :partial_mark
  #   t.integer     :next_question_id             # заполняется, если следующим может быть только определённый вопрос
  #   t.datetime    :deleted_at
  #   t.timestamps
  # end
  # add_index :questions, :id,                unique: true
  #
  # #Тест
  # create_table :tests do |t|
  #   t.string      :name
  #   t.text        :description
  #   t.string      :manual
  #   t.boolean     :active
  #   t.boolean     :shuffle
  #   t.time        :time_limit
  #   t.boolean     :explore_correct_answers
  #   t.boolean     :explore_correct_answers_after_stopped
  #   t.integer     :attempts_number             # количество попыток
  #   t.datetime    :started_at
  #   t.datetime    :stopped_at
  #   t.datetime    :deleted_at
  #   t.timestamps
  # end
  # add_index :tests, :id,                unique: true
  #
  #
  # #Связка теста и сета вопросов
  # create_table :test_question_sets do |t|
  #   t.string      :name
  #   t.belongs_to  :test
  #   t.belongs_to  :question_set
  #   t.integer     :sort_by
  #   t.datetime    :deleted_at
  #   t.timestamps
  # end
  # add_index :test_question_sets, :id,                unique: true
  #
  #
  # #Связка сета вопросов с самими вопросами
  # create_table :question_set_questions do |t|
  #   t.belongs_to  :question_set
  #   t.belongs_to  :question
  #   t.time        :time_limit
  #   t.boolean     :show_correct_answer
  #   t.datetime    :deleted_at
  #   t.timestamps
  # end
  # add_index :question_set_questions, :id,                unique: true
  #
  #
  # #Тест, созданный для пользователя
  # create_table :user_tests do |t|
  #   t.belongs_to  :test_id
  #   t.belongs_to  :user
  #   t.datetime    :start_time
  #   t.datetime    :end_time
  #   t.float       :primary_mark
  #   t.float       :mark100
  #   t.text        :description
  #   t.datetime    :deleted_at
  #   t.timestamps
  # end
  # add_index :user_tests, :id,                unique: true
  #
  # #Набор вопросов, подобранный для теста, созданного для пользователя
  # create_table :user_test_questions do |t|
  #   t.belongs_to  :user_test
  #   t.belongs_to  :question
  #   t.datetime    :start_time
  #   t.datetime    :end_time
  #   t.text        :user_answer
  #   t.float       :primary_mark
  #   t.marked      :bool
  #   t.datetime    :deleted_at
  #   t.timestamps
  # end
  # add_index :user_test_questions, :id,                unique: true


end

#Предмет: Квалификационные испытания экспертов, Русский язык, ОГЭ

