class DropTableMonitoring < ActiveRecord::Migration[6.1]
  def change
    drop_table :monitoring
  end
end
