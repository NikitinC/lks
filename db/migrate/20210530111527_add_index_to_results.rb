class AddIndexToResults < ActiveRecord::Migration[6.1]
  add_index :results, [:test_type_id, :identifier], unique: true, name: 'result_test_type_identfier'
end
