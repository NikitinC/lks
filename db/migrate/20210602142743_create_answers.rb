class CreateAnswers < ActiveRecord::Migration[6.1]
  def change
    create_table :answers do |t|
      t.belongs_to :test_type
      t.date :examdate
      t.belongs_to :subject
      t.string  :identifier, :limit => 40
      t.integer :task_number
      t.string  :answer_value, :limit => 100
      t.string  :replace_value, :limit => 100
      t.timestamps
    end
    add_index :answers, :id,                unique: true
    add_index :answers, [:test_type_id, :examdate, :subject_id], name: 'result_id'
    add_index :answers, [:identifier], name: 'identifier'
  end
end
