class DropTableMouos < ActiveRecord::Migration[6.1]
  def change
    drop_table :mouos
    create_table :mouos do |t|
      t.belongs_to :ate, foreign_key: true
      t.integer :code
      t.string  :name
      t.string  :contact_fio
      t.string  :contact_phone
      t.timestamps
    end

  end
end
