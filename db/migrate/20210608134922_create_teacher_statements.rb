class CreateTeacherStatements < ActiveRecord::Migration[6.1]
  def change
    create_table :teacher_statements do |t|
      t.string :fio
      t.string :email
      t.belongs_to :ate
      t.string :phone
      t.belongs_to :ou
      t.string :job_place
      t.boolean :subject01
      t.boolean :subject02
      t.boolean :subject03
      t.boolean :subject04
      t.boolean :subject05
      t.boolean :subject06
      t.boolean :subject07
      t.boolean :subject08
      t.string :subjects_other
      t.boolean :napr01
      t.boolean :napr02
      t.boolean :napr03
      t.boolean :napr04
      t.boolean :napr05
      t.boolean :napr06
      t.boolean :napr07
      t.boolean :napr08
      t.string :napr_other
      t.integer :cifra
      t.boolean :cifra_napr01
      t.boolean :cifra_napr02
      t.boolean :cifra_napr03
      t.string :cifra_other
      t.string :individual
      t.boolean :form01
      t.boolean :form02
      t.boolean :form03
      t.boolean :form04
      t.boolean :form05
      t.string :form_other
      t.belongs_to :user
      t.timestamps
    end
  end
end
