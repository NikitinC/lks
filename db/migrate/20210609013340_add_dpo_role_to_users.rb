class AddDpoRoleToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :dpo_role, :boolean, default: true
  end
end
