class AddPhonesToRegistrationPlaces < ActiveRecord::Migration[6.1]
  def change
    add_column :registration_places, :phones, :string
    add_column :registration_places, :site, :string
    add_column :registration_places, :code, :string
  end
end
