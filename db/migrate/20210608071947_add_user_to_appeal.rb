class AddUserToAppeal < ActiveRecord::Migration[6.1]
  def change
    add_belongs_to :appeals, :user
  end
end
