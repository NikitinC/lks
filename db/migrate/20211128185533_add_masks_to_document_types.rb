class AddMasksToDocumentTypes < ActiveRecord::Migration[6.1]
  def change
    add_column :document_types, :series_mask, :string
    add_column :document_types, :number_mask, :string
    add_column :document_types, :series_example, :string
    add_column :document_types, :number_example, :string
    add_column :document_types, :fct_ege_code, :integer
    add_column :document_types, :fct_oge_code, :integer
    add_column :document_types, :fias_code, :integer
  end
end
