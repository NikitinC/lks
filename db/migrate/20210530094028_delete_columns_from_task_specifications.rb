class DeleteColumnsFromTaskSpecifications < ActiveRecord::Migration[6.1]
  def change
    remove_column :task_specification, :competention_type
    remove_column :task_specification, :competention_name
    remove_column :task_specification, :zun_name
  end
end
