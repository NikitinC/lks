class CreateEgeStatements < ActiveRecord::Migration[6.1]
  def change

    create_table :registration_places do |t|
      t.belongs_to :ate, null: false, foreign_key: true
      t.string :name
      t.string :address
      t.datetime :deleted_at
      t.timestamps
    end

    create_table :citizenships do |t|
      t.string :code
      t.string :name
      t.string :short_name
      t.integer :fisgia9code
      t.integer :fisgia11code
      t.integer :sort_by
      t.datetime :deleted_at
      t.timestamps
    end
    add_index :citizenships, :code

    create_table :document_types do |t|
      t.string :code
      t.string :name
      t.string :short_name
      t.integer :fisgia9code
      t.integer :fisgia11code
      t.integer :sort_by
      t.datetime :deleted_at
      t.timestamps
    end
    add_index :document_types, :code

    create_table :ege_participant_categories do |t|
      t.string :code
      t.string :name
      t.string :short_name
      t.integer :fisgia9code
      t.integer :fisgia11code
      t.integer :sort_by
      t.datetime :deleted_at
      t.timestamps
    end
    add_index :ege_participant_categories, :code

    create_table :regions do |t|
      t.string :code
      t.string :name
      t.string :short_name
      t.integer :fisgia9code
      t.integer :fisgia11code
      t.integer :sort_by
      t.datetime :deleted_at
      t.timestamps
    end
    add_index :regions, :code

    create_table :exam_waves do |t|
      t.string :code
      t.string :name
      t.string :short_name
      t.integer :fisgia9code
      t.integer :fisgia11code
      t.integer :sort_by
      t.datetime :deleted_at
      t.timestamps
    end
    add_index :exam_waves, :code

    create_table :exams do |t|
      t.belongs_to :exam_wave, null: false, foreign_key: true
      t.belongs_to :subject, null: false, foreign_key: true
      t.string :name
      t.integer :fisgia11code
      t.integer :sort_by
      t.datetime :deleted_at
      t.timestamps
    end

    create_table :ege_statements do |t|
      t.belongs_to :user, null: false, foreign_key: true
      t.belongs_to :registration_place, null: false, foreign_key: true
      t.string :surname
      t.string :name
      t.string :second_name
      t.belongs_to :document_type, null: false, foreign_key: true
      t.string :document_series
      t.string :document_number
      t.date :birthday
      t.boolean :gender
      t.belongs_to :citizenship, null: false, foreign_key: true
      t.string :snils
      t.string :phone
      t.belongs_to :ege_participant_category, null: false, foreign_key: true
      t.boolean :gia_active_results_rus
      t.boolean :gia_active_results_math
      t.belongs_to :region, null: false, foreign_key: true
      t.string :education_scan
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
