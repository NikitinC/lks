class AddBlankRoleToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :blank_role, :boolean, default: true
  end
end