<?php
    function getPasswordHash($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    $arguments = getopt("s:");

    echo (password_hash($arguments['s'], PASSWORD_DEFAULT));
?>