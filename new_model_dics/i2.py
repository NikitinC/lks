import tkinter as tk
import shutil
import os
from os import walk
# import fileinput

#rails generate scaffold Educations code:integer name:string short_name:string fisgia9code:integer fisgia11code:integer sort_by:integer deleted_at:datetime - генерация БД
# :code, :name, :created_at, :updated_at, :short_name, :fisgia9code, :fisgia11code, :sort_by

def ensure_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)

def ccc(source, destination):
   # try:
   #    shutil.copy(source, destination)
   #    print("File copied successfully.")
 
   # except shutil.SameFileError:
   #    print("Source and destination represents the same file.")
 
   # except PermissionError:
   #    print("Permission denied.")

   # except:
   #    print("Error occurred while copying file.")
   shutil.copy(source, destination)
   # print(source)
   # print(destination)

#

# git add ../app/models/question_types_import.rb
# git add ../app/models/question_type.rb
# git add ../app/controllers/question_types_imports_controller.rb
# git add ../app/controllers/question_types_controller.rb
# git add ../app/views/question_types/show.html.haml
# git add ../app/views/question_types/_list.html.haml
# git add ../app/views/question_types/index.html.haml
# git add ../app/views/question_types/_form.html.haml
# git add ../app/views/question_types/_edit.html.haml
# git add ../app/views/question_types/edit.html.haml
# git add ../app/views/question_types/index.xlsx.rb
# git add ../app/views/question_types/new.html.haml
# git add ../app/views/question_types_imports/create.html.haml
# git add ../app/views/question_types_imports/new.html.haml
# git add ../config/locales/question_types/ru.yml

newmodel_name = 'question_type'
newmodel_name_big = 'QuestionType'

oldmodel_name = 'answer_type'
oldmodel_name_big = 'AnswerType'

print("Creating:", newmodel_name)

for r, d, f in os.walk('model2change'):
    for file in f:
      # try:
        fname = os.path.join(r, file)
        outdir = '../' + os.path.join(r).replace('model2change/', '').replace(oldmodel_name, newmodel_name)
        outname = outdir + '/' + file

        outname = outname.replace(oldmodel_name, newmodel_name)
        outname = outname.replace('ys_', 'ies_')
        print (outname)
        if '.rb' in outname or '.html.haml' in outname or '.yml' in outname:
          # print (outdir)
          ensure_dir(outdir)
          # shutil.copy(fname, outname)
          # Read in the file
          with open(fname, 'r') as file :
            filedata = file.read()

            # Replace the target string
            filedata = filedata.replace(oldmodel_name, newmodel_name)
            filedata = filedata.replace(oldmodel_name_big, newmodel_name_big)
            filedata = filedata.replace('ys_', 'ies_')
            if newmodel_name[-1] == 'y':
                filedata = filedata.replace(newmodel_name_big + 's', newmodel_name_big + 'ies')

            # Write the file out again
            with open(outname, 'w') as file:
              file.write(filedata)
      # except:
      #   print ("Not copying", f)  
      #   ensure_dir(outname)
      #   shutil.copy(fname, outname)

# ensure_dir('./out/'+newmodel_name + '/controllers')
# ccc('./model2change/controllers/results_controller.rb', './out/' + newmodel_name + '/comntrollers/' + newmodel_name + 's_controller.rb')
