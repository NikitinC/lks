class AnswerTypesImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @answer_types_import = AnswerTypesImport.new
  end

  def create
    @answer_types_import = AnswerTypesImport.new(params[:answer_types_import])
    render :create
  end

end
