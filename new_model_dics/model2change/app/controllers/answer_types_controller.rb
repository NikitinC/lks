class AnswerTypesController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @answer_types = AnswerType.accessible_by(current_ability).paginate(page: params[:page]).order('created_at DESC')
    if current_user.admin?
      if !params[:search].nil?
        @answer_types = AnswerType.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('created_at DESC')
      end
    else
      if !params[:search].nil?
        @answer_types = AnswerType.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('created_at DESC')
      end
    end
  end

  def export
    @answer_types = AnswerType.accessible_by(current_ability).order('created_at DESC')
    render xlsx: 'answer_types_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/answer_types/export.xlsx.axlsx'
    # render "export.xlsx.axlsx"
  end

  def new
    @answer_type = AnswerType.new
    if !@answer_type.nil?
      render :edit
    else
      flash[:success] = t("cctvs.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def show
    @answer_type = AnswerType.accessible_by(current_ability).find(params[:id])
    @previous = AnswerType.accessible_by(current_ability).where("id < " + @answer_type.id.to_s).order("id DESC").first
    @next = AnswerType.accessible_by(current_ability).where("id > " + @answer_type.id.to_s).order("id ASC").first
    @answer_types = AnswerType.accessible_by(current_ability).all.order('created_at DESC')
    if can? :view, @answer_type
      render :action => 'show'
    end
  end

  def create
    @answer_type = AnswerType.new(answer_type_params)
    if @answer_type.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @answer_type = AnswerType.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @answer_type }
    end
  end

  def modaledit
    @answer_type = AnswerType.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @answer_type }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    AnswerType.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @answer_type = AnswerType.accessible_by(current_ability).find(params[:id])
    @answer_type.destroy
    respond_to do |format|
      format.html { redirect_to answer_types_url, notice: t("answer_types.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @answer_type = AnswerType.accessible_by(current_ability).find(params[:id])

    if @answer_type.update answer_type_params
      respond_to do |format|
        format.html {
          flash[:success] = t("answer_types.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @answer_type.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("answer_types." + er.attribute.to_s) + ': ' + t("answer_types.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@answer_type) }
      end
    end

  end

  def answer_type_params
    params.require(:answer_type).permit(:name, :deleted_at, :created_at, :updated_at)
  end

  private
  def undo_link
    view_context.link_to("undo", revert_version_path(@answer_type.versions.scoped.last), :method => :post)
  end

end
