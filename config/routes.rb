require 'sidekiq/web'
# root 'pages#home'
# skip_before_action :authenticate_user!, :only => [:index]

Rails.application.routes.draw do
#   devise_for :users
#   devise_for :users, controllers: { ... , sessions: "sessions", ... }

  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end

  # mount RailsAdmin::Engine => '/rails_admin', :as => 'rails_admin'

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  namespace :admin do
    resources :users
    resources :announcements
    resources :notifications
    resources :services
    root to: "users#index"
  end

  get '/ates/export', to: 'ates#export'
  resources :ates
  resources :ates_imports, only: [:new, :create]

  resources :anonces

  get '/objectivity_ate_marks/export', to: 'objectivity_ate_marks#export'
  resources :objectivity_ate_marks
  resources :objectivity_ate_marks_imports, only: [:new, :create]

  get '/objectivity_ou_marks/export', to: 'objectivity_ou_marks#export'
  resources :objectivity_ou_marks
  resources :objectivity_ou_marks_imports, only: [:new, :create]


  get '/monitoring_objectivity_ous/export', to: 'monitoring_objectivity_ous#export'
  resources :monitoring_objectivity_ous
  resources :monitoring_objectivity_ous_imports, only: [:new, :create]

  get '/monitoring_objectivity_ates/export', to: 'monitoring_objectivity_ates#export'
  resources :monitoring_objectivity_ates
  resources :monitoring_objectivity_ates_imports, only: [:new, :create]

  get '/mouos/export', to: 'mouos#export'
  resources :mouos
  resources :mouos_imports, only: [:new, :create]

  get '/ous/export', to: 'ous#export'
  resources :ous
  resources :ous_imports, only: [:new, :create]

  get '/results/export', to: 'results#export'
  resources :results
  resources :results_imports, only: [:new, :create, :index]

  get '/answers/export', to: 'answers#export'
  resources :answers
  resources :answers_imports, only: [:new, :create]

  get '/subjects/export', to: 'subjects#export'
  resources :subjects
  resources :subjects_imports, only: [:new, :create]

  get '/process_conditions/export', to: 'process_conditions#export'
  resources :process_conditions
  resources :process_conditions_imports, only: [:new, :create]

  get '/test_types/export', to: 'test_types#export'
  resources :test_types
  resources :test_types_imports, only: [:new, :create]

  get '/task_specifications/export', to: 'task_specifications#export'
  resources :task_specifications
  resources :task_specifications_imports, only: [:new, :create]

  get '/teacher_statements/export', to: 'teacher_statements#export'
  resources :teacher_statements
  resources :teacher_statements_imports, only: [:new, :create]

  get '/task_recomendations/export', to: 'task_recomendations#export'
  resources :task_recomendations
  resources :task_recomendations_imports, only: [:new, :create]

  get '/user_test_types/export', to: 'user_test_types#export'
  resources :user_test_types
  resources :user_test_types_imports, only: [:new, :create]

  get '/appeals/export', to: 'appeals#export'
  resources :appeals
  resources :appeals_imports, only: [:new, :create]

  get '/appeal_conditions/export', to: 'appeal_conditions#export'
  resources :appeal_conditions
  resources :appeal_conditions_imports, only: [:new, :create]

  get '/appeal_types/export', to: 'appeal_types#export'
  resources :appeal_types
  resources :appeal_types_imports, only: [:new, :create]

  get '/appeal_stations/export', to: 'appeal_stations#export'
  resources :appeal_stations
  resources :appeal_stations_imports, only: [:new, :create]

  get '/my_apellations', to: 'home#my_apellations'
  get '/apellations', to: 'home#apellations'
  get '/privacy', to: 'home#privacy'
  get '/help', to: 'home#help'
  get '/terms', to: 'home#terms'
  get '/show_result/:id', to: 'home#show_result'
  get '/show_recomendations/:id', to: 'home#show_recomendations'
  get '/show_appeals/:id', to: 'home#show_appeals'
  get '/show_blank/:id', to: 'home#show_blank'
  get '/show_results_list', to: 'home#show_results_list'
  get '/monitoring_objectivity_ates_imports', to: 'monitoring_objectivity_ates_imports#new'
  get '/monitoring_objectivity_ous_imports', to: 'monitoring_objectivity_ous_imports#new'
  get '/monitoring_objectivity_ous_imports', to: 'monitoring_objectivity_ous_imports#new'

  authenticate :user, lambda { |u| u.admin? } do
    mount Sidekiq::Web => '/sidekiq'
  end

  resources :notifications, only: [:index]
  resources :announcements, only: [:index]

  resources :show_blanks, only: [:index, :show]
  resources :youtube, only: :show
  resources :tables, only: [:show, :create, :update]

  # Для регистрации
  get '/citizenships/export', to: 'citizenships#export'
  get '/citizenships/modaledit/:id', to: 'citizenships#modaledit'
  resources :citizenships
  resources :citizenships_imports, only: [:new, :create, :index]

  get '/document_types/export', to: 'document_types#export'
  get '/document_types/modaledit/:id', to: 'document_types#modaledit'
  resources :document_types
  resources :document_types_imports, only: [:new, :create, :index]

  get '/regions/export', to: 'regions#export'
  get '/regions/modaledit/:id', to: 'regions#modaledit'
  resources :regions
  resources :regions_imports, only: [:new, :create, :index]

  get '/ege_participant_categories/export', to: 'ege_participant_categories#export'
  get '/ege_participant_categories/modaledit/:id', to: 'ege_participant_categories#modaledit'
  resources :ege_participant_categories
  resources :ege_participant_categories_imports, only: [:new, :create, :index]

  get '/exams/export', to: 'exams#export'
  get '/exams/modaledit/:id', to: 'exams#modaledit'
  resources :exams
  resources :exams_imports, only: [:new, :create, :index]

  get '/exam_waves/export', to: 'exam_waves#export'
  get '/exam_waves/modaledit/:id', to: 'exam_waves#modaledit'
  resources :exam_waves
  resources :exam_waves_imports, only: [:new, :create, :index]

  get '/registration_places/export', to: 'registration_places#export'
  get '/registration_places/modaledit/:id', to: 'registration_places#modaledit'
  resources :registration_places
  resources :registration_places_imports, only: [:new, :create, :index]

  get '/ege_statements/export', to: 'ege_statements#export'
  get '/ege_statements/modaledit/:id', to: 'ege_statements#modaledit'
  resources :ege_statements
  resources :ege_statements_imports, only: [:new, :create, :index]
  get '/ege_statements/get_registration_places', to: "ege_statements#get_registration_places"
  get '/get_registration_places', to: "ege_statements#get_registration_places"

  get '/answer_types/export', to: 'answer_types#export'
  get '/answer_types/modaledit/:id', to: 'answer_type#modaledit'
  resources :answer_types
  resources :answer_types_imports, only: [:new, :create, :index]

  get '/question_types/export', to: 'question_types#export'
  get '/question_types/modaledit/:id', to: 'question_type#modaledit'
  resources :question_types
  resources :question_types_imports, only: [:new, :create, :index]

  devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks" }
  # devise_for :users, path_names: {sign_in: "login", sign_out: "logout"}
  # root :to => redirect("/users/sign_in")

  root :to => 'home#index'

  #
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
