  get '/question_types/export', to: 'question_types#export'
  get '/question_types/modaledit/:id', to: 'question_type#modaledit'
  resources :question_types
  resources :question_types_imports, only: [:new, :create, :index]