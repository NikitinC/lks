require "active_support/core_ext/integer/time"
# Rails.application.routes.default_url_options[:host] = "monitor.gia66.ru"

Rails.application.configure do
  # config.action_mailer.default_url_options = { host: 'monitor.gia66.ru', port: 3000 }
  # config.action_mailer.default_url_options = { :host => "lk.gia66.ru" }
  # config.assets.precompile += ['rails_admin/rails_admin.css', 'rails_admin/rails_admin.js']
  config.action_mailer.default_url_options = { :host => "blankoge.yanao.ru" }
  config.action_mailer.perform_deliveries = true
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
      tls: true,
      address: "smtp.yandex.com",
      port: 465,
      domain: "yandex.com",
      authentication: "plain",
      enable_starttls_auto: true,
      user_name: 'reg@edzsoft.ru',
      password: 'passw0rdreg'
  }
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Ensures that a master key has been made available in either ENV["RAILS_MASTER_KEY"]
  # or in config/master.key. This key is used to decrypt credentials (and other encrypted files).
  # config.require_master_key = true

  # Disable serving static files from the `/public` folder by default since
  # Apache or NGINX already handles this.
  # config.public_file_server.enabled = ENV['RAILS_SERVE_STATIC_FILES'].present?
  config.public_file_server.enabled configures = true

  # Compress CSS using a preprocessor.
  config.assets.css_compressor = :sass
  config.assets.js_compressor = :terser
  # config.assets.precompile += ['rails_admin/rails_admin.css', 'rails_admin/rails_admin.js']

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = false

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.asset_host = 'http://assets.example.com'

  # config.action_mailer.default_url_options = { :host => 'monitor.gia66.ru' }

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = 'X-Sendfile' # for Apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for NGINX

  # Store uploaded files on the local file system (see config/storage.yml for options).
  config.active_storage.service = :local

  # Mount Action Cable outside main process or domain.
  # config.action_cable.mount_path = nil
  # config.action_cable.url = 'wss://example.com/cable'
  # config.action_cable.default_url_options = { :host => 'monitor.gia66.ru' }
  # config.action_cable.allowed_request_origins = [ 'http://example.com', /http:\/\/example.*/ ]

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # Include generic and useful information about system operation, but avoid logging too much
  # information to avoid inadvertent exposure of personally identifiable information (PII).
  config.log_level = :info

  # Prepend all log lines with the following tags.
  config.log_tags = [ :request_id ]

  # Use a different cache store in production.
  # config.cache_store = :mem_cache_store

  # Use a real queuing backend for Active Job (and separate queues per environment).
  # config.active_job.queue_adapter     = :resque
  # config.active_job.queue_name_prefix = "monitoring_production"

  config.action_mailer.perform_caching = false

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  # config.action_mailer.raise_delivery_errors = false

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Log disallowed deprecations.
  config.active_support.disallowed_deprecation = :log

  # Tell Active Support which deprecation messages to disallow.
  config.active_support.disallowed_deprecation_warnings = []

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Use a different logger for distributed setups.
  # require "syslog/logger"
  # config.logger = ActiveSupport::TaggedLogging.new(Syslog::Logger.new 'app-name')

  if ENV["RAILS_LOG_TO_STDOUT"].present?
    logger           = ActiveSupport::Logger.new(STDOUT)
    logger.formatter = config.log_formatter
    config.logger    = ActiveSupport::TaggedLogging.new(logger)
  end

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false

  # Inserts middleware to perform automatic connection switching.
  # The `database_selector` hash is used to pass options to the DatabaseSelector
  # middleware. The `delay` is used to determine how long to wait after a write
  # to send a subsequent read to the primary.
  #
  # The `database_resolver` class is used by the middleware to determine which
  # database is appropriate to use based on the time delay.
  #
  # The `database_resolver_context` class is used by the middleware to set
  # timestamps for the last write to the primary. The resolver uses the context
  # class timestamps to determine how long to wait before reading from the
  # replica.
  #
  # By default Rails will store a last write timestamp in the session. The
  # DatabaseSelector middleware is designed as such you can define your own
  # strategy for connection switching and pass that into the middleware through
  # these configuration options.
  # config.active_record.database_selector = { delay: 2.seconds }
  # config.active_record.database_resolver = ActiveRecord::Middleware::DatabaseSelector::Resolver
  # config.active_record.database_resolver_context = ActiveRecord::Middleware::DatabaseSelector::Resolver::Session
end

#
# drop table if exists action_text_rich_texts cascade;
# drop table if exists appeals cascade;
# drop table if exists answers cascade;
# drop table if exists appeal_conditions cascade;
# drop table if exists appeal_stations cascade;
# drop table if exists appeal_types cascade;
# drop table if exists objectivity_ate_marks cascade;
# drop table if exists objectivity_ou_marks cascade;
# drop table if exists process_conditions cascade;
# drop table if exists results cascade;
# drop table if exists subjects cascade;
# drop table if exists task_recomendations cascade;
# drop table if exists task_specification cascade;
# drop table if exists test_types cascade;
# drop table if exists user_test_types cascade;
# drop table if exists sql_packages cascade;
# drop table if exists sql_features cascade;
# drop table if exists sql_implementation_info cascade;
# drop table if exists sql_parts cascade;
# drop table if exists sql_languages cascade;
# drop table if exists sql_sizing cascade;
# drop table if exists sql_sizing_profiles cascade;
# drop table if exists teacher_statements cascade;
#
#
# drop table if exists ous cascade;
# drop table if exists notifications cascade;
# drop table if exists mouos cascade;
# drop table if exists monitoring_objectivity_ous cascade;
# drop table if exists monitoring_objectivity_ates cascade;
# drop table if exists ates cascade;
# drop table if exists announcements cascade;
#
# drop table if exists active_storage_attachments cascade;
# drop table if exists active_storage_blobs cascade;
# drop table if exists active_storage_variant_records cascade;
# drop table if exists ar_internal_metadata cascade;
# drop table if exists friendly_id_slugs cascade;
# drop table if exists services cascade;
# drop table if exists schema_migrations cascade;
# drop table if exists users cascade;
